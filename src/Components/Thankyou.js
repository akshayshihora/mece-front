import React, { Component } from 'react';
import { Link } from "react-router-dom";
import {Image} from 'react-bootstrap';


class Thankyou extends Component {

  render() {
    return (
      
        <div className="thanks-div">
          <div class="col-md-12">
            <div class="text-xs-center">
              <h1 class="text-center txt-thanks">Thank You!</h1>
              <p class="thankyou-p1 text-center">Thank you for your submission. MECE Inc. team member will contact you shortly.</p>
              <hr class="hr-thanks" />
            </div>

            <div class="col-md-offset-1 col-md-10 col-md-offset-1" style={{marginTop:'-2%'}}>
              <p class="thankyou-p1">MECE Inc. will contact you with any additional information required and the status of firms being contacted.</p>
              <p class="thankyou-p3"> Submissions from the firms to review. </p>
            </div>
            
          </div>
        </div>
      
    );
  }
}

export default Thankyou;
