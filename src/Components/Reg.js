import React from 'react';
import "react-datepicker/dist/react-datepicker.css";
import './reg.css';
import DatePicker from "react-datepicker";
import { Registration } from './Services/Registration';
import { Col, Row, Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
let errors = {

};
let formIsValid = false;
class Reg extends React.Component {
    services = ["Marketing", "Sales", "Risk", "Accounting", "Analytics", "Inovation", "Corporate Development", "Supply Chain", "Finance", "Strategy", "Program Management", "Procurement", "Tax", "Sustainability", "Real Estate", "Technology", "Human Resources"];
    subservices = ["Pricing & Profitabilty Management", "Pricing Strategy ", "Customer & Marketing Strategy", "Customer Insights & Behavourial analytics", "Customer Journey Mapping", "Customer Experience Strategy and Vision", "Markeing Exceution", "Customer Segmenatation", "Brand Management", "Markeing Investment Optimization", "Trade Promotion", "Customer experience capability development"];
    constructor(props) {
        super(props);
        console.log(this.props.location.data);
        //    if(typeof this.props.location.data === 'undefined')
        //        {
        //            this.props.history.push({pathname: '/'})
        //        }
        this.state = {
            contact_name: '',
            contact_email: '',
            company_name: '',
            objective: '',
            deadline_timestamp: '',
            budget: '',
            selected_firm_ids: this.props.location.data,
            selecteddate: '',
            errors: {}
        }
        this.Submit = this.Submit.bind(this);
        this.onChange = this.onChange.bind(this);


    }
    onChange(e) {
        let name = e.target.name;
        let value = e.target.value;
        console.log(name + " " + value);
        if (typeof value !== "undefined") {

            if (name == 'username') {
                if (value != '') {
                    formIsValid = true;
                    errors.username = "";
                } else {
                    formIsValid = false;
                    errors.username = "Enter Valid value";
                }
            }
            if (name == 'companyname') {
                if (value != '') {
                    formIsValid = true;
                    errors.companyname = "";
                } else {
                    formIsValid = false;
                    errors.companyname = "Enter Valid value";
                }
            }
            if (name == 'deadline_timestamp') {
                if (value != '') {
                    formIsValid = true;
                    errors.deadline_timestamp = "";
                } else {
                    formIsValid = false;
                    errors.deadline_timestamp = "Enter Valid value";
                }
            }
            if (name == 'objective') {
                if (value != '') {
                    formIsValid = true;
                    errors.objective = "";
                } else {
                    formIsValid = false;
                    errors.objective = "Enter Valid value";
                }
            }
            if (name == 'budget') {

                if (!parseInt(value)) {
                    formIsValid = false;
                    errors.budget = "Only letters.";
                } else {
                    formIsValid = true;
                    errors.budget = "";
                }
            }
            if (name == 'emailid') {
                let lastAtPos = value.lastIndexOf('@');
                let lastDotPos = value.lastIndexOf('.');

                if (!(lastAtPos < lastDotPos && lastAtPos > 0 && value.indexOf('@@') == -1 && lastDotPos > 2 && (value.length - lastDotPos) > 2)) {
                    console.log("not valid");
                    formIsValid = false;
                    errors["emailid"] = "Email is not valid.";
                } else {
                    console.log("valid");
                    formIsValid = true;
                    errors["emailid"] = "";
                }
            }
        }

        //       if(errors.length>0)
        //       {

        if (e.target.name == 'deadline_timestamp') {
            var myDate = e.target.value;//2019-02-28
            myDate = myDate.split("-");
            var newDate = myDate[1] + "," + myDate[2] + "," + myDate[0];
            var newDate_val = myDate[1] + "/" + myDate[2] + "/" + myDate[0];
            const new_value = new Date(newDate).getTime();
            this.setState({ [e.target.name]: new_value });
            //            alert(newDate_val);
            //            this.setState({selecteddate: ''})
            this.setState({ selecteddate: newDate_val });
            console.log(this.state);
        } else {
            console.log(e.target.name);
            console.log(e.target.value);
            this.setState({ [e.target.name]: e.target.value });
            console.log(this.state);
        }
        //       }
        //       else
        //       {
        //            console.log(errors);
        //            this.setState({errors:errors});
        //        }
    }

    Submit() {
        console.log(formIsValid);
        console.log("data", this.state);
        if (this.state.errors.emailid == "" && this.state.errors.budget == '' && this.state.errors.objective == '' && this.state.errors.companyname == '' && this.state.errors.deadline_timestamp == '' && this.state.errors.username == '') {
            return fetch('https://mycluster-328113.us-south.containers.appdomain.cloud/mece/api/v1/transaction/860a3124e86bba966d01d2685e147eeb', {
                method: 'Put',
                body: JSON.stringify(this.state),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(response => {
                if (response.status == 200) {
                    this.props.history.push({
                        pathname: '/Thankyou',
                    })
                } else {
                    console.log('Somthing happened wrong');
                }
            }).catch(err => err);
            console.log("catch");
        } else {
            alert("Enter Valid values.")
            this.setState({ errors: errors });
        }

    }
    handleChange(event) {

        var options = event.target.options;
        var value = [];
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value.push(options[i].value);
                this.setState({ [event.target.name]: value });
            }
        }

        this.setState({ value: event.target.value });
    }
    componentDidMount() {
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">

                    <div className="col-md-12 col-sm-12" style={{ display: 'none' }}>
                        <div className="col-md-6 col-sm-6" >
                            <div className="register">
                                <div className="formheader" >
                                    <h3 style={{ padding: '23px', fontSize: '30px' }}>Personal Information</h3>
                                </div>
                                <form >
                                    <label className="textsize-contact">Name*</label>
                                    <input type="text" name="username" className="form-control" placeholder="Full name" />

                                    <label className="textsize-contact">Email*</label>
                                    <input type="text" className="form-control" name="emailid" placeholder="Company e-mail" />

                                    <label className="textsize-contact">Phone*</label>
                                    <input type="text" name="mobileno" className="form-control" placeholder="xxx-xxx-xxx" />

                                    <br />
                                    <span className="note">Note:Personal Information will NOT be shared with potential firms</span>

                                </form>
                            </div>
                        </div>



                        <div className="col-md-6 col-sm-6" >
                            <div className="register1">
                                <div className="formheader" >
                                    <h3 style={{ padding: '23px', fontSize: '30px' }}>Company Information</h3>
                                </div>
                                <div>
                                    <form>
                                        <label className="textsize-contact">Company Name*</label>
                                        <input type="text" name="username" className="form-control" placeholder="Company name" />
                                        <label className="textsize-contact">Industry*</label>
                                        <select className="form-control">
                                            <option value="Enter Industry Name">Industry</option>
                                            <option value="AUTOMOBILES">AUTOMOBILES</option>
                                            <option value="INSURANCE">INSURANCE</option>
                                            <option value="RAILWAYS">RAILWAYS</option>
                                            <option value="SERVICES">SERVICES</option>
                                        </select>
                                        <label className="textsize-contact">Maturity*</label>
                                        <select className="form-control">
                                            <option value="Maturity">Maturity</option>
                                            <option value="volvo">Volvo</option>
                                            <option value="saab">Saab</option>
                                            <option value="mercedes">Mercedes</option>
                                            <option value="audi">Audi</option>
                                        </select>
                                        <label className="textsize-contact">Size*</label>
                                        <select className="form-control">
                                            <option value="#employees">#employees</option>
                                            <option value="2">2</option>
                                            <option value="4">4</option>
                                            <option value="6">6</option>
                                            <option value="8">8</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="rowproject" >
                    <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12" >
                        <div className="register2">
                            <h3 className="formheader1">Information</h3>
                            <hr className="projectborder" />
                            <form onSubmit={this.handleSubmit}>
                                <div className="row">

                                    <div className="col-md-6 col-sm-6" >
                                        <div className="form-group">
                                            <label className="textsize-contact">Full Name*</label>
                                            <input type="text" name="username" className="form-control" placeholder="Full name" onChange={this.onChange} />
                                            <span style={{ color: "red" }}>{this.state.errors.username}</span>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-6" >
                                        <div className="form-group">
                                            <label className="textsize-contact">Professional Email*</label>
                                            <input type="text" className="form-control" name="emailid" placeholder="Professional Email" onChange={this.onChange} />
                                            <span style={{ color: "red" }}>{this.state.errors.emailid}</span>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-6" >
                                        <div className="form-group">
                                            <label className="textsize-contact">Company*</label>
                                            <input type="text" name="companyname" className="form-control" placeholder="Company name" onChange={this.onChange} />
                                            <span style={{ color: "red" }}>{this.state.errors.companyname}</span>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-6" >
                                        <div className="form-group">
                                            <label className="textsize-contact">Project scope / objective*</label>
                                            <FormControl className="textarea" componentClass="textarea" name="objective" placeholder="(1)(2)(3)" onChange={this.onChange} />
                                            <span style={{ color: "red" }}>{this.state.errors.objective}</span>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-6" >
                                        <div className="form-group">
                                            <label className="textsize-contact">Response deadline*</label>
                                            <input value={this.state.selecteddate} type="date" placeholder="mm/dd/yyyy" className="form-control" style={{ height: '38px' }} onChange={this.onChange} name="deadline_timestamp" />
                                            <span style={{ color: "red" }}>{this.state.errors.deadline_timestamp}</span>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-6" >
                                        <div className="form-group">
                                            <label className="textsize-contact">Budget</label>
                                            <input type="text" name="budget" className="form-control" placeholder="Approx.amount alloted for the project" onChange={this.onChange} />
                                            <span style={{ color: "red" }}>{this.state.errors.budget}</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="success cnt-success ">
                    <Button type="button" className="btn-contact" bsStyle="" bsSize="large" style={{ paddingLeft: '50%', paddingRight: '50%', marginTop: '22%', backgroundColor: '#4dac4c', color: 'white' }} onClick={this.Submit}> Submit</Button>
                </div>

                <div class="referesh-submission">
                    <div className="col-md-12 col-sm-12">
                        <h3 class="referesh">Click here to contact us for questions.</h3>
                        <div className="click">
                            <Link to='/Contactnew'><Button type="button" className="button" >Contact us</Button></Link>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
export default Reg;