import React from 'react';
import "react-datepicker/dist/react-datepicker.css";
import './reg.css';
import { MultiSelect } from 'react-selectize';
import { RefData } from './Services/RefData';
import { Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import $ from 'jquery';

var year = "";
var month = "";
class Reference extends React.Component {
  services = ["Marketing", "Sales", "Risk", "Accounting", "Analytics", "Inovation", "Corporate Development", "Supply Chain", "Finance", "Strategy", "Program Management", "Procurement", "Tax", "Sustainability", "Real Estate", "Technology", "Human Resources"];
  subservices = ["Pricing & Profitabilty Management", "Pricing Strategy ", "Customer & Marketing Strategy", "Customer Insights & Behavourial analytics", "Customer Journey Mapping", "Customer Experience Strategy and Vision", "Markeing Exceution", "Customer Segmenatation", "Brand Management", "Markeing Investment Optimization", "Trade Promotion", "Customer experience capability development"];

  constructor(props) {
    super(props);
    this.state = {
      firmname: '',
      client_name: '',
      client_industry: '',
      client_stage: '',
      fee_range: {
        min: '',
        max: ''
      },
      client_revenue_range: {
        minimum: '',
        maximum: ''
      },
      service_areas: [],
      services: [],
      project_length: '',
      year_completed: '',
      feedback: '',
      serviceArea: [],
      subservices: [],
      companyIndustries: [],
      "companyAll": [],
      "rangesall": []


    }
    this.ReferenceData = this.ReferenceData.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.serviceArea = this.serviceArea.bind(this);
    this.getIdustries = this.getIdustries.bind(this);
    this.ranges = this.ranges.bind(this);
    this.RevanueRanges = this.RevanueRanges.bind(this);
  }
  onChange(e) {
    console.log(e.target.name);
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
    console.log("this is state", this.state);
  }

  handleChange(optionsList) {

    console.log(optionsList);
    const new_array = [];
    const new_sub_services = [];
    optionsList.map((k1) => {
      new_array.push(k1.label)
    });
    this.setState({ service_areas: new_array });
    this.state.serviceArea.map(function (myservice1) {
      if (new_array.includes(myservice1.service_area)) {
        myservice1.services.map(function (item) {
          if (!new_sub_services.includes(item.service)) {
            new_sub_services.push(item.service);
          }
        })
      }
    })
    this.setState({ subservices: new_sub_services });
    //    var options = event.target.options;
    //    var value = [];
    //    for (var i = 0, l = options.length; i < l; i++) {
    //        if (options[i].selected) {
    //          value.push(options[i].value);
    //
    //        }
    //    }
    //console.log(event.target.value);
    //  this.setState({value: event.target.value});
  }
  subserviceClicked(optionsList) {
    console.log(optionsList);
    const new_array = [];
    optionsList.map((k1) => {
      new_array.push(k1.label)
    });
    this.setState({ services: new_array });
  }
  getIdustries() {


    var getTypes = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/industries";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getTypes,
      success: function (data) {
        data = '[ {"industry": "Aerospace & Defense","description": ""}, { "industry": "Automotive & Assembly","description": "" }, {  "industry": "Capital Projects & Infrastructure", "description": "" }, {"industry": "Chemicals","description": "" }, { "industry": "Consumer Packaged Goods","description": "" }, {"industry": "Electric Power & Natural Gas (Utilities)", "description": "" }, {  "industry": "Financial Services (incl. Insurance)", "description": ""  },  { "industry": "Healthcare Systems & Services",  "description": ""  }, { "industry": "Hospitality & Tourism", "description": "" }, {  "industry": "High Tech",  "description": "" }, { "industry": "Manufacturing","description": "" }, {  "industry": "Media & Entertainment Metals & Mining", "description": ""},  { "industry": "Oil & Gas",  "description": "" },  {"industry": "Agriculture, Forestry, & Fishing", "description": "" },   { "industry": "Pharmaceuticals & Life Sciences",  "description": "" }, { "industry": "Private Equity & Principal investors","description": "" }, { "industry": "Government & Public Agencies",  "description": "" }, { "industry": "Retail & Wholesale","description": ""  }, {"industry": "Semiconductors","description": "" },{"industry": "Social Sector","description": "" }, {  "industry": "Telecommunications", "description": ""  },  { "industry": "Transportation & Logistics","description": ""  }, { "industry": "Education", "description": ""  }, { "industry": "Arts, Entertainment, & Recreation", "description": "" }, {"industry": "Business Services & Agency","description": "" }, {"industry": "Personal Services",  "description": "" }]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ companyIndustries: myObject });

      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }
  serviceArea() {
    var getservice = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getservice,
      success: function (data) {
        // data = '{ "service_areas": [ { "services": [ { "service": "Artificial Intelligence"  }, {  "service": "Big Data Analytics"   }, {  "service": "Business Intelligence"   },  {  "service": "Cloud" },   {"service": "Digital Transformation"    },  { "service": "Internet of Things (IoT)" }  ], "service_area": "Analytics" }, {"services": [  { "service": "Business Process Redesign"}, {  "service": "Center of Excellence (CoE)" },{ "service": "Outsourcing"   },{"service": "Shared Services"} ],"service_area": "Business Process" },  {"services": [{"service": "Culture Change Management"}, {"service": "Facilitation" },{"service": "Organizational Change Management" },  {"service": "Training"} ],  "service_area": "Change Management" }, { "services": [{"service": "Leadership Coaching" }  ], "service_area": "Coaching"},  {"services": [ {  "service": "Corporate Finance" },{ "service": "Crisis and Restructuring"  },{"service": "Divestiture" }, { "service": "Initial Public Offering (IPO)" },  {"service": "Mergers & Acquisitions (M&A)"}, { "service": "Post-Transaction" } ],  "service_area": "Corporate Development/ Structure"},{ "services": [ { "service": "Process Improvement" }, { "service": "Regulatory"  },  { "service": "Staffing"    }, {"service": "Strategy" },  {"service": "Technology"    }, { "service": "Treasury"  }],"service_area": "Finance & Accounting"},{"services": [{"service": "Growth Strategy"  }, {  "service": "New Business" },{"service": "Organizational Innovation" }],"service_area": "Innovation" }, {"services": [{  "service": "Financial Assessment" }  ], "service_area": "Legal"},{"services": [ { "service": "Actuarial" },{"service": "Human Resources Management"},{"service": "Organizational Design" }, {  "service": "Rewards, Compensation, and Benefits"},{"service": "Staffing"}, {"service": "Talent Acquisition/Recruiting"}, {"service": "Talent Management"}, {"service": "Technology"} ], "service_area": "People & Organization (HR)"}, {"services": [{"service": "Communication Planning"}, {"service": "Health Check/ Quality Assurance"}, {"service": "Leadership"}, {"service": "Program Management"}, {"service": "Project Management"} ], "service_area": "Project Management"}, {"services": [{"service": "Facility Planning"}, {"service": "Strategy"} ], "service_area": "Real Estate"}, {"services": [{"service": "Business & Reputation Risk"}, {"service": "Cyber Security"}, {"service": "Financial Risk"}, {"service": "Operational Risk"}, {"service": "Outsourcing"}, {"service": "Quality"}, {"service": "Regulatory"}, {"service": "Technology"} ], "service_area": "Risk"}, {"services": [{"service": "Marketing"}, {"service": "Pricing"}, {"service": "Sales"}, {"service": "Staffing"} ], "service_area": "Sales & Marketing"}, {"services": [{"service": "Board leadership"}, {"service": "Business Model"}, {"service": "Product Design & Management"}, {"service": "Strategy"} ], "service_area": "Strategy"}, {"services": [{"service": "Capital Projects"}, {"service": "Center of Excellence (CoE)"}, {"service": "Inventory/ Product"}, {"service": "Lean Transformation"}, {"service": "Logistics & Distribution"}, {"service": "Managed Services"}, {"service": "Operations"}, {"service": "Procurement"}, {"service": "Safety"}, {"service": "Supply Chain Management"}, {"service": "Technology"} ], "service_area": "Supply Chain & Operations"}, {"services": [{"service": "Social Impact"}, {"service": "Sustainability"} ], "service_area": "Sustainability"}, {"services": [{"service": "Common Reporting Standard"}, {"service": "Crisis and Restructuring"}, {"service": "Employer Services"}, {"service": "Indirect Tax"}, {"service": "International Tax"}, {"service": "Investment & Innovation Tax"}, {"service": "Multistate Tax"}, {"service": "Private Wealth"}, {"service": "Process Improvement"}, {"service": "Staffing"}, {"service": "Transfer Pricing"} ], "service_area": "Tax"}, {"services": [{"service": "Application Management Services"}, {"service": "Blockchain"}, {"service": "Cloud"}, {"service": "Microsoft"}, {"service": "Mobile development"}, {"service": "NetSuite"}, {"service": "Oracle"}, {"service": "Quality & Compliance"}, {"service": "Robotics"}, {"service": "Salesforce"}, {"service": "SAP"}, {"service": "Software Design and Development"}, {"service": "Staffing"}, {"service": "Technology Strategy & Architecture"}, {"service": "Web Development"} ], "service_area": "Technology"} ] }'; 

        data = '{"service_areas": [{"services": [{"service": "Artificial Intelligence"}, {"service": "Big Data Analytics"}, {"service": "Business Intelligence"}, {"service": "Cloud"}, {"service": "Digital Transformation"}, {"service": "Internet of Things (IoT)"} ], "service_area": "Analytics"}, {"services": [{"service": "Business Process Redesign"}, {"service": "Center of Excellence (CoE)"}, {"service": "Outsourcing"}, {"service": "Shared Services"} ], "service_area": "Business Process"}, {"services": [{"service": "Culture Change Management"}, {"service": "Facilitation"}, {"service": "Organizational Change Management"}, {"service": "Training"} ], "service_area": "Change Management"}, {"services": [{"service": "Leadership Coaching"} ], "service_area": "Coaching"}, {"services": [{"service": "Corporate Finance"}, {"service": "Crisis and Restructuring"}, {"service": "Divestiture"}, {"service": "Initial Public Offering (IPO)"}, {"service": "Mergers & Acquisitions (M&A)"}, {"service": "Post-Transaction"} ], "service_area": "Corporate Development/ Structure"}, {"services": [{"service": "Process Improvement"}, {"service": "Regulatory"}, {"service": "Staffing"}, {"service": "Strategy"}, {"service": "Technology"}, {"service": "Treasury"} ], "service_area": "Finance & Accounting"}, {"services": [{"service": "Growth Strategy"}, {"service": "New Business"}, {"service": "Organizational Innovation"} ], "service_area": "Innovation"}, {"services": [{"service": "Financial Assessment"} ], "service_area": "Legal"}, {"services": [{"service": "Actuarial"}, {"service": "Human Resources Management"}, {"service": "Organizational Design"}, {"service": "Rewards, Compensation, and Benefits"}, {"service": "Staffing"}, {"service": "Talent Acquisition/Recruiting"}, {"service": "Talent Management"}, {"service": "Technology"} ], "service_area": "People & Organization (HR)"}, {"services": [{"service": "Communication Planning"}, {"service": "Health Check/ Quality Assurance"}, {"service": "Leadership"}, {"service": "Program Management"}, {"service": "Project Management"} ], "service_area": "Project Management"}, {"services": [{"service": "Facility Planning"}, {"service": "Strategy"} ], "service_area": "Real Estate"}, {"services": [{"service": "Business & Reputation Risk"}, {"service": "Cyber Security"}, {"service": "Financial Risk"}, {"service": "Operational Risk"}, {"service": "Outsourcing"}, {"service": "Quality"}, {"service": "Regulatory"}, {"service": "Technology"} ], "service_area": "Risk"}, {"services": [{"service": "Marketing"}, {"service": "Pricing"}, {"service": "Sales"}, {"service": "Staffing"} ], "service_area": "Sales & Marketing"}, {"services": [{"service": "Board leadership"}, {"service": "Business Model"}, {"service": "Product Design & Management"}, {"service": "Strategy"} ], "service_area": "Strategy"}, {"services": [{"service": "Capital Projects"}, {"service": "Center of Excellence (CoE)"}, {"service": "Inventory/ Product"}, {"service": "Lean Transformation"}, {"service": "Logistics & Distribution"}, {"service": "Managed Services"}, {"service": "Operations"}, {"service": "Procurement"}, {"service": "Safety"}, {"service": "Supply Chain Management"}, {"service": "Technology"} ], "service_area": "Supply Chain & Operations"}, {"services": [{"service": "Social Impact"}, {"service": "Sustainability"} ], "service_area": "Sustainability"}, {"services": [{"service": "Common Reporting Standard"}, {"service": "Crisis and Restructuring"}, {"service": "Employer Services"}, {"service": "Indirect Tax"}, {"service": "International Tax"}, {"service": "Investment & Innovation Tax"}, {"service": "Multistate Tax"}, {"service": "Private Wealth"}, {"service": "Process Improvement"}, {"service": "Staffing"}, {"service": "Transfer Pricing"} ], "service_area": "Tax"}, {"services": [{"service": "Application Management Services"}, {"service": "Blockchain"}, {"service": "Cloud"}, {"service": "Microsoft"}, {"service": "Mobile development"}, {"service": "NetSuite"}, {"service": "Oracle"}, {"service": "Quality & Compliance"}, {"service": "Robotics"}, {"service": "Salesforce"}, {"service": "SAP"}, {"service": "Software Design and Development"}, {"service": "Staffing"}, {"service": "Technology Strategy & Architecture"}, {"service": "Web Development"} ], "service_area": "Technology"} ] }'
        var myObject = JSON.parse(data);
        //  var allMeasurement
        //  alert(data);
        this.setState({ serviceArea: myObject['service_areas'] });
        //console.log(data);
      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }
    });


  }
  ReferenceData() {
    if (this.state) {
      if (this.state.completion_date && this.state.client_name && this.state.client_industry && this.state.client_industry && this.state.project_length) {
        console.log("first if block");

        $('.spanner').show();
        $.ajax({
          type: 'POST',
          mode: 'no-cors',
          headers: {
            'Content-Type': "application/json",
            'Access-Control-Allow-Origin': '*',
          },
          data: JSON.stringify(this.state),
          url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/mece/api/v1/reference',
          success: function (response) {
            alert("Data saved Successfully");
            this.props.history.push({
              pathname: '/',
              data: response // your data array of objects
            })
            $('.spanner').hide();
          }.bind(this),
          error: function (xhr, status, err) {
            this.props.history.push({
              pathname: '/',
            })
            $('.spanner').hide();
          }.bind(this),
        });
      }
      else {
        alert("Enter valid data");
      }
    }
  }



  ranges() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/company-stages',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ companyAll: data.stages });
        console.log("get", this.state.companyAll)

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });
  }


  RevanueRanges() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/revenue-ranges',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ rangesall: data.ranges });
        console.log("getrev", this.state.rangesall)

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });
  }

  componentDidMount() {
    this.RevanueRanges();
    this.serviceArea();
    this.getIdustries();
    this.ranges();
    // var min = new Date().getFullYear(),
    // max = min + 9,
    // select = document.getElementById('year-drop');

    // for (var i = min; i<=max; i++){
    // var opt = document.createElement('option');
    // opt.value = i;
    // opt.innerHTML = i;
    // select.appendChild(opt);
    // }
    $(document).ready(function () {
      var d = new Date();
      month = d.getMonth() + 1;
      year = d.getFullYear();

      $("#month").val(month);
      $("#year").val(year);
    });
  }

  render() {
    if (this.state.companyIndustries.length > 0) {
      console.log(this.state.companyIndustries);
      var industryies = this.state.companyIndustries.map(function (industryies) {
        return <option value={industryies.industry}>{industryies.industry}</option>
      })
    }
    if (this.state.companyAll.length > 0) {
      var opt = this.state.companyAll.map((opt) => {
        return <option value={opt}>{opt}</option>
      })
    }

    if (this.state.rangesall.length > 0) {
      console.log(this.state.rangesall);
      var minimumrenges = this.state.rangesall.map(function (ranges) {
        return <option value={ranges.minimum}>{ranges.minimum}</option>
      })
    }

    if (this.state.rangesall.length > 0) {
      console.log(this.state.rangesall);
      var maximumrenges = this.state.rangesall.map(function (ranges) {
        return <option value={ranges.maximum}>{ranges.maximum}</option>
      })
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-offset-1 col-md-10 col-sm-10 col-lg-10 col-xs-10" >
            <form onSubmit={this.handleSubmit}>
              <label>Your Firm Name</label>
              <input type="text" name="firmname" className="form-control" onChange={this.onChange} />

              <label>Client name*</label>
              <input type="text" name="client_name" value={this.state.client_name} className="form-control" placeholder="" onChange={this.onChange} />

              <label>Industry*</label>
              <select className="form-control" name="client_industry" onChange={this.onChange}>
                <option value=""></option>
                {industryies}
              </select>

              <div className="row">
                <div className="col-md-12">

                  <div className="col-md-6 padleft5">
                    <label name="client_revenue_range">Annual Revenue</label>
                    {/* <input type="text" name="minimum" className="form-control" onChange={this.onChange}/> */}
                    <select name="minimum" className="form-control" onChange={this.onChange}>
                      <option value="">please select minimum revenue</option>
                      {minimumrenges}
                    </select>
                  </div>

                  <div className="col-md-6 padright5">
                    <label style={{ visibility: 'hidden' }}>Completion date</label>
                    {/* <input type="text" name="maximum" className="form-control" onChange={this.onChange}/> */}
                    <select name="maximum" className="form-control" onChange={this.onChange}>
                      <option value="">please select maximum revenue</option>
                      {maximumrenges}
                    </select>
                  </div>
                </div>

              </div>

              <label>Company stage</label>
              <select name="client_stage" className="form-control" onChange={this.onChange}>
                <option value="">please select company stage</option>
                {opt}
              </select>
              <label>Services*</label>
              <MultiSelect
                style={{ marginTop: '5px' }}
                className="select-one"

                name="service"
                onValuesChange={this.handleChange.bind(this)}
                options={this.state.serviceArea.map(
                  service => ({ label: service.service_area, value: service.service_area })
                )}
              />

              <label>Sub-Services*</label>

              <MultiSelect
                style={{ marginTop: '5px' }}
                className="select-one"

                name="subservice"
                onValuesChange={this.subserviceClicked.bind(this)}
                options={this.state.subservices.map(
                  service => ({ label: service, value: service })
                )}
              />
              <div className="row">
                <div className="col-md-12">

                  <div className="col-md-6 padleft5">
                    <label name="fee_range">Total Fee</label>
                    <input type="text" name="min" className="form-control" onChange={this.onChange} />
                  </div>
                  <div className="col-md-6 padright5">
                    <label style={{ visibility: 'hidden' }}>Completion date</label>
                    <input type="text" name="max" className="form-control" onChange={this.onChange} />
                  </div>
                </div>

              </div>

              <div className="row">
                <div className="col-md-12">
                  <div className="col-md-6 padleft5">
                    <label>Project length*</label>
                    <select className="form-control " name="project_length" onChange={this.onChange}>
                      <option value="">Month</option>
                      <option value="01">01</option>
                      <option value="02">02</option>
                      <option value="03">03</option>
                      <option value="04">04</option>
                      <option value="05">05</option>
                      <option value="06">06</option>
                      <option value="07">07</option>
                      <option value="08">08</option>
                      <option value="09">09</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">

                  <div className="col-md-6 padleft5">
                    <label>Completion date*</label>
                    <select className="form-control " name="completion_date" onChange={this.onChange} id="month">

                      <option value="1">January</option>
                      <option value="2">February</option>
                      <option value="3">March</option>
                      <option value="4">April</option>
                      <option value="5">May</option>
                      <option value="6">June</option>
                      <option value="7">July</option>
                      <option value="8">August</option>
                      <option value="9">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                    </select>
                  </div>
                  <div className="col-md-6 padright5">
                    <label style={{ visibility: 'hidden' }}>Completion date</label>
                    <select className="form-control " id="year-drop" name="year_completed" onChange={this.onChange}>
                      {/* <option value="">Year</option> */}
                      <option value={year}>{year}</option>
                    </select>
                  </div>
                </div>
              </div>
              <label>Reference name</label>
              <input type="text" name="rname" className="form-control" placeholder="" onChange={this.onChange} />

              <label>Reference e-mail</label>
              <input type="text" name="email" className="form-control" placeholder="" onChange={this.onChange} />

              <FormGroup controlId="formControlsTextarea" >
                <ControlLabel>Comments</ControlLabel>
                <FormControl className="textarea" componentClass="textarea" name="feedback" onChange={this.onChange} />
              </FormGroup>

              <div>
                <center>
                  <Button type="button" bsStyle="" className="btn-contact btn btn-lg btn-success" bsSize="large" onClick={this.ReferenceData}>Submit</Button>
                </center>
              </div>
            </form>
          </div>
        </div>
      </div>

    );
  }
}
export default Reference;