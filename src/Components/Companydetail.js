
import React, { Component } from 'react';
import { Image } from 'react-bootstrap';
import $ from 'jquery';
import { CardBody, CardTitle } from 'reactstrap';
import StarRatingComponent from 'react-star-rating-component';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Pie } from 'react-chartjs-2';
import ReactSvgPieChart from "react-svg-piechart"
import { black } from 'material-ui/styles/colors';
//import PieChart from 'react-simple-pie-chart';
var label_array = [];
var percentage_array = [];

var label_array_service = [];
var percentage_array_service = [];
var back_array = [];
var label_industry_expertise = [];
var perecentage_industry_expertise = [];

class Companydetail extends Component {
  constructor(props) {
    super(props);
    console.log("helloall", this.props)
    if (typeof this.props.location.state === 'undefined') {
      this.props.history.push({ pathname: '/' })
    }
    console.log("chart", props.location.state.selected);
    var foo = props.location.state.selected;

    console.log("foo",foo.statistics);


   
    var companies = props.location.state.companies;

   companies.map( (locat,i)=> {
      
    console.log("index",i);
    console.log("index22", locat.name);
        
      })
      
   //   console.log ("companiesst",companies[index].name);
    
    var selected_services = props.location.state.selected_service;
    console.log("selected_sub",selected_services);
    var selected_subservices = props.location.state.selected_service_area;
    var total_sub = props.location.state.selcted_sub;

    var statatictic = props.location.state;

    console.log("checkedstatic",statatictic);

    // var heq = companies.map(function (locat) {
      
    // var  heq = locat.statistics
    //   console.log("akshay12",heq);
     
    // })
  

      if(selected_subservices!="") {
      //  alert("akshay");
          label_array = [];
          percentage_array = [];
          foo.statistics.client_revenue_stage.map((key, value) => {
            label_array.push(key.label);
      
            percentage_array.push(key.percentage);
      
            // percentage_array.push(key.percentage);
      
          });
          label_array_service = [];
          percentage_array_service = [];
      
          foo.statistics.service_area.map((key, value) => {
            label_array_service.push(key.label);
            //   label_array_service.push("");
            //  percentage_array_service.push(key.percentage);
            percentage_array_service.push(key.percentage);
      
          });
      
          label_industry_expertise = [];
          perecentage_industry_expertise = [];
          foo.statistics.industry_expertise.map((key, value) => {
            label_industry_expertise.push(key.label);
            //    label_industry_expertise.push("");
            perecentage_industry_expertise.push(key.percentage);
            //  perecentage_industry_expertise.push("");
          });
          
      }
      else {
   //   alert("shihora");
      }
     
  
          
      
        
  
  
    this.state = {
      companyData: foo,
      companyData1: {},
      allcompanies: { 'companies': companies },
      selected_services: selected_services,
      selected_subservices: selected_subservices,
      total_sub: total_sub,
      rating: 1,

      dataPie: {
        labels: label_array,

        datasets: [

          {
            data: percentage_array,
            backgroundColor: [
              "#F7464A",
              "#46BFBD",
              "#FDB45C",
              "#949FB1",
              "#4D5360"

            ],

            hoverBackgroundColor: [
              "#FF5A5E",
              "#5AD3D1",
              "#FFC870",
              "#A8B3C5",
              "#616774"

            ]
          }
        ]
      },

      dataPie1: {

        labels: label_array_service,


        datasets: [
          {

            data: percentage_array_service,
            backgroundColor: [
              "#F7464A",
              "#46BFBD",
              "#FDB45C",
              "#949FB1",
              "#4D5360"

            ],
            hoverBackgroundColor: [
              "#FF5A5E",
              "#5AD3D1",
              "#FFC870",
              "#A8B3C5",
              "#616774"

            ],
          }

        ]
      },

      dataPie2: {
        labels: label_industry_expertise,

        datasets: [
          {
            data: perecentage_industry_expertise,

            backgroundColor: [
              "#F7464A",
              "#46BFBD",
              "#FDB45C",
              "#949FB1",
              "#4D5360"

            ],
            hoverBackgroundColor: [
              "#FF5A5E",
              "#5AD3D1",
              "#FFC870",
              "#A8B3C5",
              "#616774"

            ],

            width: [
              "20px"
            ]
          }
        ]
      }
    }
  }


  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }
  getCompanyData() {
    $.ajax({
      url: '/companyDetail.json',
      dataType: 'json',
      cache: false,
      success: function (data) {
        this.setState({ companyData1: data });
        console.log("dataget", this.state.companyData1)
      }.bind(this),
      error: function (xhr, status, err) {
        console.log("dataget1", err);
        alert(err);
      }
    });
  }

  componentDidMount() {

    $(document).ready(function () {
      var originalString = $(".cust_span_tag p").text();
      var separatedArray = originalString.split(', ');

      //console.log(separatedArray); 
      //alert(separatedArray);
      for (var i = 0; i < separatedArray.length; i++) {
        $(".cust_span_tag>span").append("<span class='cust_tag'>" + separatedArray[i] + "</span>")
      }
      if ($(".cust_span_tag>span .cust_tag").text() == "") {
        $(".cust_span_tag>span .cust_tag").remove();
      }
    });



    var new_array = [];
    $.each(this.state.total_sub, function (index, value) {
      console.log("$vishal", value);
      $.each(value, function (index1, value1) {
        console.log("$vishal", value1);
        new_array.push(value1.service);

      });
    });

    back_array['response'] = this.state.allcompanies;
    back_array['services'] = this.state.selected_services;
    back_array['subservices'] = this.state.selected_subservices;
    back_array['allsubservices'] = new_array;

    console.log("$vishal", this.state.total_sub);
    console.log("$vishal", back_array);

    // this.getgraph();
    this.getCompanyData();

  }
  render() {

    const { rating } = this.state;

    var name = this.state.companyData.name
    var website = this.state.companyData.website;
    var description = this.state.companyData.description;
    var phone = this.state.companyData.contact_number;
    var email = this.state.companyData.email_address;
    var year_founded = this.state.companyData.year_founded;
    var stastics = this.state.companyData.statistics;
    var industryexpertise = this.state.companyData.industry_expertise
    var industries_served = this.state.companyData.industries_served.join(',');



    var certification = this.state.companyData.certifications.join(' , ');
    var address1 = '';
    var head_quaters = '';
    this.state.companyData.locations.map(function (location) {
      console.log("location12", location);
      if (location.is_headquarters) {
        head_quaters = location.city + ', ' + location.state + ', ' + location.country;
      }
      else {
        address1 += location.city + '  ' + location.state + '  ' + location.country + ' , ';
      }
    });

    var logo1 = this.state.companyData.company_logo_url;


    if (this.state.companyData.accolades) {
      var Accolades_recognitions = this.state.companyData.accolades;
      console.log(Accolades_recognitions);
      var Accolades_recognitions = this.state.companyData.accolades.map(function (Accolades_recognitions) {

        return <div class="col-md-6">
          {Accolades_recognitions ? Accolades_recognitions : 'No Accolades'}
        </div>

      })
    }
    if (this.state.companyData1.review) {
      var client_background = this.state.companyData1.review;
      var client_background = this.state.companyData1.review.client_background.map(function (client_background) {
        return <div class="col-md-4 background-img-div1">
          <h3 class="text-center resp-cmp-deatil" style={{ padding: '1%' }}>Client background</h3>
          <p className="cmp-client-detail">Industry:{client_background.industry}</p>
          <p className="cmp-client-detail">Size:{client_background.size}</p>
          <p className="cmp-client-detail">Stage:{client_background.stage}</p>
        </div>
      })
    }
    if (this.state.companyData1.review) {

      var project_background = this.state.companyData1.review.project_background.map(function (project_background) {
        return <div class="col-md-4 background-img-div2">
          <h3 class="text-center resp-cmp-deatil" style={{ padding: '1%' }}>Project background</h3>
          <p className="cmp-client-detail">Service(s):{project_background.service}</p>
          <p className="cmp-client-detail">Completion:{project_background.completion}</p>
          <p className="cmp-client-detail">Project length:{project_background.projectlength}</p>
          <p className="cmp-client-detail">Cost:{project_background.cost}</p>
        </div>
      })
    }
    if (this.state.companyData1.review) {

      var review_detail = this.state.companyData1.review.review_detail.map(function (review_detail) {
        return <div class="col-md-4 background-img-div3">
          <h3 class="text-center resp-cmp-deatil" style={{ padding: '1%' }}>Review details</h3>
          <p className="cmp-client-detail">Overall: <StarRatingComponent
            name="rate1"
            starCount={5}
            value={5} /></p>
          <p className="cmp-client-detail">Quality:{review_detail.quality} Schedule :{review_detail.schedule}</p>
          <p className="cmp-client-detail">Cost :{review_detail.cost}</p>
          <p className="cmp-client-detail">Highlight : {review_detail.highlight}</p>
        </div>
      })
    }

    return (
      <div className="container">
        <div className="col-md-12" style={{ marginTop: '5%', marginBottom: '3%' }}>
          <div>
            <Link to={{ pathname: '/firm_list', data: back_array }} className="text sidebar-list-more back_btn_cust" >
              Back to Result
                 </Link>
          </div>
          <div className="col-md-7 companydetail-logo-firm">
            <div className="row">
              <div className="company_detailimg col-md-6" style={{ backgroundImage: "url(" + logo1 + ")" }}></div>
              {/* <Image src={logo1} className="" alt="Company Logo" /> */}
              {/* <h2 className="frimname" style={{marginTop:'-8%',marginLeft:'39%',fontSize:'32px'}}>{name}</h2> */}

              {/* <input type ="text" name = "image" placeholder = "image upload here" />        <h2 className="frimname" style={{marginTop:'-8%',marginLeft:'39%',fontSize:'32px'}}>{name}</h2> */}
              <div className="det_const_sec col-md-6">
                <h2 className="cust_title1">{name}</h2>
                <h2>
                  <StarRatingComponent
                    name="rate1"
                    starCount={5}
                    value={5}
                    onStarClick={this.onStarClick.bind(this)} />
                  10 Reviews
              </h2>
                <h3> Description </h3>
                <p style={{ textAlign: 'justify' }}>{description}</p>

              </div>
              <div className="cust_design_lastrow col-md-12">
                <h3> Accolades & recognitions </h3>
                <div className="last_cust_cont">
                  {Accolades_recognitions}
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-5 form-div-companydetail">
            <form>
              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>NAME:</label>
                <br />
                {name}
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>
              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>WEBSITE:</label>
                <br />
                <a href={website} target="_blank">{website}</a>
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>

              {/* <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>DESCRIPTION:</label>
            <br/>
            {description}
            <hr style={{margin:'0px',padding:'0px'}} />
            </div> */}

              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>PHONE:</label>
                <br />
                {phone}
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>

              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>EMAIL:</label>
                <br />
                <a href={"mailto:" + email}>{email}</a>
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>

              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>YEAR FOUNDED:</label>
                <br />
                {year_founded}
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>

              {/* <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>Industry:</label>
            <br/>
            {industries_served}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div> */}

              <div style={{ marginLeft: '5%' }} className="cust_span_tag">
                <label style={{ color: '#999' }}>CERTIFICATION:</label>
                <br />
                <p className="noshow">{certification}</p>
                <span ></span>
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>

              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>HEADQUATERS:</label>
                <br />
                {head_quaters ? head_quaters : 'No HEADQUATERS'}
                <hr style={{ margin: '0px', padding: '0px' }} />
              </div>

              <div style={{ marginLeft: '5%' }}>
                <label style={{ color: '#999' }}>OTHER LOCATIONS:</label>
                <br />
                <span style={{ wordSpacing: "4px" }}>  {address1} </span>
                {/* <hr style={{margin:'0px',padding:'0px'}} /> */}
              </div>
            </form>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="col-md-6 chart-div chart-div-left">
              <h3 className="text-center" style={{ fontSize: '30px' }}> Firm expertise </h3>
              <div className="col-md-6" style={{ marginBottom: '6%' }}>
                <p className="text-center"> Industry expertise </p>

                <Pie data={this.state.dataPie2} options={{
                  legend: { display: false }, responsive: true, tooltips: {
                    titleSpacing: 10,
                    xPadding: 5,
                    yPadding: 5,

                    // enabled: false


                  },

                }} height={130} width={130} />

              </div>
              <div className="col-md-6" style={{ marginBottom: '6%' }}>
                <p class="text-center"> Service area </p>
                <Pie data={this.state.dataPie1} options={{
                  legend: { display: false }, responsive: true, tooltips: {
                    titleSpacing: 0,
                    xPadding: 5,
                    yPadding: 5,
                  },
                }} height='200' width='200' />
              </div>
            </div>
            <div className="col-md-6 chart-div chart-div-right">
              <h3 className="text-center" style={{ fontSize: '30px' }}> Firm focus </h3>
              <div className="col-md-6" style={{ marginBottom: '6%' }}>
                {/* <p class="text-center"> Client size </p> */}
                {/* <Pie data={this.state.dataPie} options={{ responsive: true }} height='200' width='200'  /> */}
                <p class="text-center"> Client stage </p>
                <Pie data={this.state.dataPie} options={{
                  legend: { display: false }, responsive: true, tooltips: {
                    titleSpacing: 0,
                    xPadding: 5,
                    yPadding: 5,
                  },
                }} height='200' width='200' />
              </div>
              <div className="col-md-6" style={{ marginBottom: '6%' }}>
                {/* <p class="text-center"> Client stage </p>
            <Pie data={this.state.dataPie} options={{ responsive: true }} height='200' width='200'  /> */}
              </div>
            </div>
          </div>
        </div>


        <div className="col-md-12" style={{ boxShadow: '1px 1px 2px #eae6e6, 0 0 25px #e0e0e0, 0 0 5px #c3bdbd', marginTop: '5%' }}>

          <div className="company-review-title-div">
            <h2> Review: 5.0
            <StarRatingComponent
                name="rate1"
                starCount={5}
                value={5}
                onStarClick={this.onStarClick.bind(this)}
              /> 10 Reviews</h2>
          </div>

          <div class="col-md-12 clnt-img">
            {client_background}
            {project_background}
            {review_detail}
          </div>

          <div className="company-see-more">
            <a href="javascript:void(0)"><h3 className="text-center" style={{ marginBottom: '2%' }}> See more </h3></a>
          </div>
        </div>
      </div>
    );
  }
}
export default Companydetail;
