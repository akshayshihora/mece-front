import React from 'react';
import { Button, Glyphicon, Col, Thumbnail, Grid, Row } from 'react-bootstrap';
import './reg.css';
import $ from 'jquery';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Thankyou from './Thankyou';
import Pagination from "react-js-pagination";
import axios from 'axios';

const new_array = [];
class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.location.data);

    if (typeof this.props.location.data === 'undefined') {
      this.props.history.push({ pathname: '/' })
    }
    else {
      this.props.location.data.companies.map((sidebarpagedata, index) => {
        this.props.location.data.companies[index]['applied'] = 0;
      })
    }
    this.state = {
      sidebarData: this.props.location.data,
      getData: {},
      allMeasurement: [],
      allEmp: [],
      certification: [],
      firmTypes: [],
      companyIndustries: [],
      accolades: [],
      serviceArea: [],
      allServices: [],
      selectedfirm: []
    }
    this.addCompnay = this.addCompnay.bind(this);
    this.submit = this.submit.bind(this);

  }
  componentDidMount() {

    this.getDt();
    this.getemp();
    this.getcertification();
    this.getTypes();
    this.getIdustries();
    this.accolaess();
    this.serviceArea();
    this.services();


  }


  getSidebarData() {
    $.ajax({
      url: '/sidebar.json',
      dataType: 'json',
      cache: false,
      success: function (data) {
        //        this.setState({sidebarData: data});
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(err);
        //alert(err);
      }
    });
  }

  getDt() {


    var get = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/revenue-ranges";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: get,
      success: function (response) {
        response = '[{"minimum":0,"maximum":1000000},{"minimum":1000000,"maximum":10000000},{"minimum":10000000,"maximum":100000000},{"minimum":100000000,"maximum":1000000000},{"minimum":1000000000,"maximum":10000000000},{"minimum":10000000000,"maximum":9223372036854776000}, { "minimum": 10001, "maximum": 9223372036854776000}]';
        var myObject = JSON.parse(response);
        this.setState({ allMeasurement: myObject });




      }.bind(this),

      error: function (xhr, status, err) {
        //               //alert("error block");
        console.log("error block");
        console.log(err);
      }

    });


  }
  getemp() {


    var getemp = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/employee-ranges";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        //'Accept': 'application/json',
        // 'Content-Type': ' application/json',
        // async:true,
        //dataType:'jsonp',
        //'Access-Control-Allow-Credentials' : true,
        'Access-Control-Allow-Origin': '*',
        //'Access-Control-Allow-Methods':'GET',
        //'Access-Control-Allow-Headers':'application/json',

      },
      url: getemp,
      success: function (data) {
        data = '[{"minimum":1,"maximum":10},{"minimum":11,"maximum":50},{"minimum":51,"maximum":200},{"minimum":201,"maximum":500},{"minimum":501,"maximum":1000},{"minimum":1001,"maximum":5000}, {"minimum":5001,"maximum": 10000}]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ allEmp: myObject });
      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }
  serviceArea() {


    var getservice = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getservice,
      success: function (data) {
        // data = '{ "service_areas": [ { "services": [ { "service": "Artificial Intelligence"  }, {  "service": "Big Data Analytics"   }, {  "service": "Business Intelligence"   },  {  "service": "Cloud" },   {"service": "Digital Transformation"    },  { "service": "Internet of Things (IoT)" }  ], "service_area": "Analytics" }, {"services": [  { "service": "Business Process Redesign"}, {  "service": "Center of Excellence (CoE)" },{ "service": "Outsourcing"   },{"service": "Shared Services"} ],"service_area": "Business Process" },  {"services": [{"service": "Culture Change Management"}, {"service": "Facilitation" },{"service": "Organizational Change Management" },  {"service": "Training"} ],  "service_area": "Change Management" }, { "services": [{"service": "Leadership Coaching" }  ], "service_area": "Coaching"},  {"services": [ {  "service": "Corporate Finance" },{ "service": "Crisis and Restructuring"  },{"service": "Divestiture" }, { "service": "Initial Public Offering (IPO)" },  {"service": "Mergers & Acquisitions (M&A)"}, { "service": "Post-Transaction" } ],  "service_area": "Corporate Development/ Structure"},{ "services": [ { "service": "Process Improvement" }, { "service": "Regulatory"  },  { "service": "Staffing"    }, {"service": "Strategy" },  {"service": "Technology"    }, { "service": "Treasury"  }],"service_area": "Finance & Accounting"},{"services": [{"service": "Growth Strategy"  }, {  "service": "New Business" },{"service": "Organizational Innovation" }],"service_area": "Innovation" }, {"services": [{  "service": "Financial Assessment" }  ], "service_area": "Legal"},{"services": [ { "service": "Actuarial" },{"service": "Human Resources Management"},{"service": "Organizational Design" }, {  "service": "Rewards, Compensation, and Benefits"},{"service": "Staffing"}, {"service": "Talent Acquisition/Recruiting"}, {"service": "Talent Management"}, {"service": "Technology"} ], "service_area": "People & Organization (HR)"}, {"services": [{"service": "Communication Planning"}, {"service": "Health Check/ Quality Assurance"}, {"service": "Leadership"}, {"service": "Program Management"}, {"service": "Project Management"} ], "service_area": "Project Management"}, {"services": [{"service": "Facility Planning"}, {"service": "Strategy"} ], "service_area": "Real Estate"}, {"services": [{"service": "Business & Reputation Risk"}, {"service": "Cyber Security"}, {"service": "Financial Risk"}, {"service": "Operational Risk"}, {"service": "Outsourcing"}, {"service": "Quality"}, {"service": "Regulatory"}, {"service": "Technology"} ], "service_area": "Risk"}, {"services": [{"service": "Marketing"}, {"service": "Pricing"}, {"service": "Sales"}, {"service": "Staffing"} ], "service_area": "Sales & Marketing"}, {"services": [{"service": "Board leadership"}, {"service": "Business Model"}, {"service": "Product Design & Management"}, {"service": "Strategy"} ], "service_area": "Strategy"}, {"services": [{"service": "Capital Projects"}, {"service": "Center of Excellence (CoE)"}, {"service": "Inventory/ Product"}, {"service": "Lean Transformation"}, {"service": "Logistics & Distribution"}, {"service": "Managed Services"}, {"service": "Operations"}, {"service": "Procurement"}, {"service": "Safety"}, {"service": "Supply Chain Management"}, {"service": "Technology"} ], "service_area": "Supply Chain & Operations"}, {"services": [{"service": "Social Impact"}, {"service": "Sustainability"} ], "service_area": "Sustainability"}, {"services": [{"service": "Common Reporting Standard"}, {"service": "Crisis and Restructuring"}, {"service": "Employer Services"}, {"service": "Indirect Tax"}, {"service": "International Tax"}, {"service": "Investment & Innovation Tax"}, {"service": "Multistate Tax"}, {"service": "Private Wealth"}, {"service": "Process Improvement"}, {"service": "Staffing"}, {"service": "Transfer Pricing"} ], "service_area": "Tax"}, {"services": [{"service": "Application Management Services"}, {"service": "Blockchain"}, {"service": "Cloud"}, {"service": "Microsoft"}, {"service": "Mobile development"}, {"service": "NetSuite"}, {"service": "Oracle"}, {"service": "Quality & Compliance"}, {"service": "Robotics"}, {"service": "Salesforce"}, {"service": "SAP"}, {"service": "Software Design and Development"}, {"service": "Staffing"}, {"service": "Technology Strategy & Architecture"}, {"service": "Web Development"} ], "service_area": "Technology"} ] }'; 

        data = '{"service_areas": [{"services": [{"service": "Artificial Intelligence"}, {"service": "Big Data Analytics"}, {"service": "Business Intelligence"}, {"service": "Cloud"}, {"service": "Digital Transformation"}, {"service": "Internet of Things (IoT)"} ], "service_area": "Analytics"}, {"services": [{"service": "Business Process Redesign"}, {"service": "Center of Excellence (CoE)"}, {"service": "Outsourcing"}, {"service": "Shared Services"} ], "service_area": "Business Process"}, {"services": [{"service": "Culture Change Management"}, {"service": "Facilitation"}, {"service": "Organizational Change Management"}, {"service": "Training"} ], "service_area": "Change Management"}, {"services": [{"service": "Leadership Coaching"} ], "service_area": "Coaching"}, {"services": [{"service": "Corporate Finance"}, {"service": "Crisis and Restructuring"}, {"service": "Divestiture"}, {"service": "Initial Public Offering (IPO)"}, {"service": "Mergers & Acquisitions (M&A)"}, {"service": "Post-Transaction"} ], "service_area": "Corporate Development/ Structure"}, {"services": [{"service": "Process Improvement"}, {"service": "Regulatory"}, {"service": "Staffing"}, {"service": "Strategy"}, {"service": "Technology"}, {"service": "Treasury"} ], "service_area": "Finance & Accounting"}, {"services": [{"service": "Growth Strategy"}, {"service": "New Business"}, {"service": "Organizational Innovation"} ], "service_area": "Innovation"}, {"services": [{"service": "Financial Assessment"} ], "service_area": "Legal"}, {"services": [{"service": "Actuarial"}, {"service": "Human Resources Management"}, {"service": "Organizational Design"}, {"service": "Rewards, Compensation, and Benefits"}, {"service": "Staffing"}, {"service": "Talent Acquisition/Recruiting"}, {"service": "Talent Management"}, {"service": "Technology"} ], "service_area": "People & Organization (HR)"}, {"services": [{"service": "Communication Planning"}, {"service": "Health Check/ Quality Assurance"}, {"service": "Leadership"}, {"service": "Program Management"}, {"service": "Project Management"} ], "service_area": "Project Management"}, {"services": [{"service": "Facility Planning"}, {"service": "Strategy"} ], "service_area": "Real Estate"}, {"services": [{"service": "Business & Reputation Risk"}, {"service": "Cyber Security"}, {"service": "Financial Risk"}, {"service": "Operational Risk"}, {"service": "Outsourcing"}, {"service": "Quality"}, {"service": "Regulatory"}, {"service": "Technology"} ], "service_area": "Risk"}, {"services": [{"service": "Marketing"}, {"service": "Pricing"}, {"service": "Sales"}, {"service": "Staffing"} ], "service_area": "Sales & Marketing"}, {"services": [{"service": "Board leadership"}, {"service": "Business Model"}, {"service": "Product Design & Management"}, {"service": "Strategy"} ], "service_area": "Strategy"}, {"services": [{"service": "Capital Projects"}, {"service": "Center of Excellence (CoE)"}, {"service": "Inventory/ Product"}, {"service": "Lean Transformation"}, {"service": "Logistics & Distribution"}, {"service": "Managed Services"}, {"service": "Operations"}, {"service": "Procurement"}, {"service": "Safety"}, {"service": "Supply Chain Management"}, {"service": "Technology"} ], "service_area": "Supply Chain & Operations"}, {"services": [{"service": "Social Impact"}, {"service": "Sustainability"} ], "service_area": "Sustainability"}, {"services": [{"service": "Common Reporting Standard"}, {"service": "Crisis and Restructuring"}, {"service": "Employer Services"}, {"service": "Indirect Tax"}, {"service": "International Tax"}, {"service": "Investment & Innovation Tax"}, {"service": "Multistate Tax"}, {"service": "Private Wealth"}, {"service": "Process Improvement"}, {"service": "Staffing"}, {"service": "Transfer Pricing"} ], "service_area": "Tax"}, {"services": [{"service": "Application Management Services"}, {"service": "Blockchain"}, {"service": "Cloud"}, {"service": "Microsoft"}, {"service": "Mobile development"}, {"service": "NetSuite"}, {"service": "Oracle"}, {"service": "Quality & Compliance"}, {"service": "Robotics"}, {"service": "Salesforce"}, {"service": "SAP"}, {"service": "Software Design and Development"}, {"service": "Staffing"}, {"service": "Technology Strategy & Architecture"}, {"service": "Web Development"} ], "service_area": "Technology"} ] }'
        var myObject = JSON.parse(data);
        //  var allMeasurement
        ////alert(data);
        this.setState({ serviceArea: myObject['service_areas'] });
        //console.log(data);
      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }

  services() {
    var getservices = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getservices,
      success: function (data) {
        // data = '{ "service_areas": [ { "services": [ { "service": "Artificial Intelligence"  }, {  "service": "Big Data Analytics"   }, {  "service": "Business Intelligence"   },  {  "service": "Cloud" },   {"service": "Digital Transformation"    },  { "service": "Internet of Things (IoT)" }  ], "service_area": "Analytics" }, {"services": [  { "service": "Business Process Redesign"}, {  "service": "Center of Excellence (CoE)" },{ "service": "Outsourcing"   },{"service": "Shared Services"} ],"service_area": "Business Process" },  {"services": [{"service": "Culture Change Management"}, {"service": "Facilitation" },{"service": "Organizational Change Management" },  {"service": "Training"} ],  "service_area": "Change Management" }, { "services": [{"service": "Leadership Coaching" }  ], "service_area": "Coaching"},  {"services": [ {  "service": "Corporate Finance" },{ "service": "Crisis and Restructuring"  },{"service": "Divestiture" }, { "service": "Initial Public Offering (IPO)" },  {"service": "Mergers & Acquisitions (M&A)"}, { "service": "Post-Transaction" } ],  "service_area": "Corporate Development/ Structure"},{ "services": [ { "service": "Process Improvement" }, { "service": "Regulatory"  },  { "service": "Staffing"    }, {"service": "Strategy" },  {"service": "Technology"    }, { "service": "Treasury"  }],"service_area": "Finance & Accounting"},{"services": [{"service": "Growth Strategy"  }, {  "service": "New Business" },{"service": "Organizational Innovation" }],"service_area": "Innovation" }, {"services": [{  "service": "Financial Assessment" }  ], "service_area": "Legal"},{"services": [ { "service": "Actuarial" },{"service": "Human Resources Management"},{"service": "Organizational Design" }, {  "service": "Rewards, Compensation, and Benefits"},{"service": "Staffing"}, {"service": "Talent Acquisition/Recruiting"}, {"service": "Talent Management"}, {"service": "Technology"} ], "service_area": "People & Organization (HR)"}, {"services": [{"service": "Communication Planning"}, {"service": "Health Check/ Quality Assurance"}, {"service": "Leadership"}, {"service": "Program Management"}, {"service": "Project Management"} ], "service_area": "Project Management"}, {"services": [{"service": "Facility Planning"}, {"service": "Strategy"} ], "service_area": "Real Estate"}, {"services": [{"service": "Business & Reputation Risk"}, {"service": "Cyber Security"}, {"service": "Financial Risk"}, {"service": "Operational Risk"}, {"service": "Outsourcing"}, {"service": "Quality"}, {"service": "Regulatory"}, {"service": "Technology"} ], "service_area": "Risk"}, {"services": [{"service": "Marketing"}, {"service": "Pricing"}, {"service": "Sales"}, {"service": "Staffing"} ], "service_area": "Sales & Marketing"}, {"services": [{"service": "Board leadership"}, {"service": "Business Model"}, {"service": "Product Design & Management"}, {"service": "Strategy"} ], "service_area": "Strategy"}, {"services": [{"service": "Capital Projects"}, {"service": "Center of Excellence (CoE)"}, {"service": "Inventory/ Product"}, {"service": "Lean Transformation"}, {"service": "Logistics & Distribution"}, {"service": "Managed Services"}, {"service": "Operations"}, {"service": "Procurement"}, {"service": "Safety"}, {"service": "Supply Chain Management"}, {"service": "Technology"} ], "service_area": "Supply Chain & Operations"}, {"services": [{"service": "Social Impact"}, {"service": "Sustainability"} ], "service_area": "Sustainability"}, {"services": [{"service": "Common Reporting Standard"}, {"service": "Crisis and Restructuring"}, {"service": "Employer Services"}, {"service": "Indirect Tax"}, {"service": "International Tax"}, {"service": "Investment & Innovation Tax"}, {"service": "Multistate Tax"}, {"service": "Private Wealth"}, {"service": "Process Improvement"}, {"service": "Staffing"}, {"service": "Transfer Pricing"} ], "service_area": "Tax"}, {"services": [{"service": "Application Management Services"}, {"service": "Blockchain"}, {"service": "Cloud"}, {"service": "Microsoft"}, {"service": "Mobile development"}, {"service": "NetSuite"}, {"service": "Oracle"}, {"service": "Quality & Compliance"}, {"service": "Robotics"}, {"service": "Salesforce"}, {"service": "SAP"}, {"service": "Software Design and Development"}, {"service": "Staffing"}, {"service": "Technology Strategy & Architecture"}, {"service": "Web Development"} ], "service_area": "Technology"} ] }'; 

        data = '{"service_areas": [{"services": [{"service": "Artificial Intelligence"}, {"service": "Big Data Analytics"}, {"service": "Business Intelligence"}, {"service": "Cloud"}, {"service": "Digital Transformation"}, {"service": "Internet of Things (IoT)"} ], "service_area": "Analytics"}, {"services": [{"service": "Business Process Redesign"}, {"service": "Center of Excellence (CoE)"}, {"service": "Outsourcing"}, {"service": "Shared Services"} ], "service_area": "Business Process"}, {"services": [{"service": "Culture Change Management"}, {"service": "Facilitation"}, {"service": "Organizational Change Management"}, {"service": "Training"} ], "service_area": "Change Management"}, {"services": [{"service": "Leadership Coaching"} ], "service_area": "Coaching"}, {"services": [{"service": "Corporate Finance"}, {"service": "Crisis and Restructuring"}, {"service": "Divestiture"}, {"service": "Initial Public Offering (IPO)"}, {"service": "Mergers & Acquisitions (M&A)"}, {"service": "Post-Transaction"} ], "service_area": "Corporate Development/ Structure"}, {"services": [{"service": "Process Improvement"}, {"service": "Regulatory"}, {"service": "Staffing"}, {"service": "Strategy"}, {"service": "Technology"}, {"service": "Treasury"} ], "service_area": "Finance & Accounting"}, {"services": [{"service": "Growth Strategy"}, {"service": "New Business"}, {"service": "Organizational Innovation"} ], "service_area": "Innovation"}, {"services": [{"service": "Financial Assessment"} ], "service_area": "Legal"}, {"services": [{"service": "Actuarial"}, {"service": "Human Resources Management"}, {"service": "Organizational Design"}, {"service": "Rewards, Compensation, and Benefits"}, {"service": "Staffing"}, {"service": "Talent Acquisition/Recruiting"}, {"service": "Talent Management"}, {"service": "Technology"} ], "service_area": "People & Organization (HR)"}, {"services": [{"service": "Communication Planning"}, {"service": "Health Check/ Quality Assurance"}, {"service": "Leadership"}, {"service": "Program Management"}, {"service": "Project Management"} ], "service_area": "Project Management"}, {"services": [{"service": "Facility Planning"}, {"service": "Strategy"} ], "service_area": "Real Estate"}, {"services": [{"service": "Business & Reputation Risk"}, {"service": "Cyber Security"}, {"service": "Financial Risk"}, {"service": "Operational Risk"}, {"service": "Outsourcing"}, {"service": "Quality"}, {"service": "Regulatory"}, {"service": "Technology"} ], "service_area": "Risk"}, {"services": [{"service": "Marketing"}, {"service": "Pricing"}, {"service": "Sales"}, {"service": "Staffing"} ], "service_area": "Sales & Marketing"}, {"services": [{"service": "Board leadership"}, {"service": "Business Model"}, {"service": "Product Design & Management"}, {"service": "Strategy"} ], "service_area": "Strategy"}, {"services": [{"service": "Capital Projects"}, {"service": "Center of Excellence (CoE)"}, {"service": "Inventory/ Product"}, {"service": "Lean Transformation"}, {"service": "Logistics & Distribution"}, {"service": "Managed Services"}, {"service": "Operations"}, {"service": "Procurement"}, {"service": "Safety"}, {"service": "Supply Chain Management"}, {"service": "Technology"} ], "service_area": "Supply Chain & Operations"}, {"services": [{"service": "Social Impact"}, {"service": "Sustainability"} ], "service_area": "Sustainability"}, {"services": [{"service": "Common Reporting Standard"}, {"service": "Crisis and Restructuring"}, {"service": "Employer Services"}, {"service": "Indirect Tax"}, {"service": "International Tax"}, {"service": "Investment & Innovation Tax"}, {"service": "Multistate Tax"}, {"service": "Private Wealth"}, {"service": "Process Improvement"}, {"service": "Staffing"}, {"service": "Transfer Pricing"} ], "service_area": "Tax"}, {"services": [{"service": "Application Management Services"}, {"service": "Blockchain"}, {"service": "Cloud"}, {"service": "Microsoft"}, {"service": "Mobile development"}, {"service": "NetSuite"}, {"service": "Oracle"}, {"service": "Quality & Compliance"}, {"service": "Robotics"}, {"service": "Salesforce"}, {"service": "SAP"}, {"service": "Software Design and Development"}, {"service": "Staffing"}, {"service": "Technology Strategy & Architecture"}, {"service": "Web Development"} ], "service_area": "Technology"} ] }'
        var myObject = JSON.parse(data);
        //  var allMeasurement
        //alert(data);
        this.setState({ allServices: myObject['service_areas'] });
        //console.log(data);
      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }
  getcertification() {
    var getcertification = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/certifications";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getcertification,
      success: function (data) {
        data = ' [ "Women owned","Minority owned"]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ certification: myObject });

      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }

  getTypes() {
    var getTypes = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/firm-types";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getTypes,
      success: function (data) {
        data = '["Educational Institution", "Government Agency","Non-profit","Partnership", "Privately Held","Public Company","Self-Employed","Sole Proprietorship"]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ firmTypes: myObject });
      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }
    });
  }
  getIdustries() {
    var getTypes = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/industries";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getTypes,
      success: function (data) {
        data = '[ {"industry": "Aerospace & Defense","description": ""}, { "industry": "Automotive & Assembly","description": "" }, {  "industry": "Capital Projects & Infrastructure", "description": "" }, {"industry": "Chemicals","description": "" }, { "industry": "Consumer Packaged Goods","description": "" }, {"industry": "Electric Power & Natural Gas (Utilities)", "description": "" }, {  "industry": "Financial Services (incl. Insurance)", "description": ""  },  { "industry": "Healthcare Systems & Services",  "description": ""  }, { "industry": "Hospitality & Tourism", "description": "" }, {  "industry": "High Tech",  "description": "" }, { "industry": "Manufacturing","description": "" }, {  "industry": "Media & Entertainment Metals & Mining", "description": ""},  { "industry": "Oil & Gas",  "description": "" },  {"industry": "Agriculture, Forestry, & Fishing", "description": "" },   { "industry": "Pharmaceuticals & Life Sciences",  "description": "" }, { "industry": "Private Equity & Principal investors","description": "" }, { "industry": "Government & Public Agencies",  "description": "" }, { "industry": "Retail & Wholesale","description": ""  }, {"industry": "Semiconductors","description": "" },{"industry": "Social Sector","description": "" }, {  "industry": "Telecommunications", "description": ""  },  { "industry": "Transportation & Logistics","description": ""  }, { "industry": "Education", "description": ""  }, { "industry": "Arts, Entertainment, & Recreation", "description": "" }, {"industry": "Business Services & Agency","description": "" }, {"industry": "Personal Services",  "description": "" }]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ companyIndustries: myObject });
      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }
  accolaess() {
    var getData = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/accolades";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getData,
      success: function (data) {
        data = '[  "Inc 5000", "Consulting Magazine - Fastest growing firms", "Consulting Magazine - Best firms to work for","Forbes - Best management consulting firms","Vault - Top management consulting firms" ]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ accolades: myObject });

      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }
    });
  }
  initMap() {

  }


  addCompnay(id, index) {

    const final_companies = this.state.sidebarData;
    if (final_companies['companies'][index]['applied'] == 0) {
      let s = 0;
      final_companies['companies'].forEach(function (e) {
        console.log(e['applied']);
        if (e['applied'] == 1) {
          s++;
        }
      });
      if (s < 5) {
        console.log(s);
        final_companies['companies'][index]['applied'] = 1;
        this.setState({ sidebarData: final_companies });
        new_array.push(id)
        this.setState({ selectedfirm: new_array });
      }
      else {
        alert("You can selecte upto 5 firms only.")
      }

    }
    else {
      final_companies['companies'][index]['applied'] = 0;
      this.setState({ sidebarData: final_companies });
      var index = new_array.indexOf(id);
      if (index !== -1) new_array.splice(index, 1);
      this.setState({ selectedfirm: new_array });
    }

  }

  submit() {
    //    alert("submitted");
    console.log(this.state.selectedfirm);
    if (this.state.selectedfirm.length > 0) {
      this.props.history.push({
        pathname: '/reg',
        data: this.state.selectedfirm // your data array of objects
      })
    }
    else {
      alert("Please select firms first");
    }
  }


  render() {
    if (this.state.allMeasurement.length > 0) {
      console.log(this.state.allMeasurement);
      var measurement = this.state.allMeasurement.map(function (measurement) {
        return <li>{measurement.minimum}-{measurement.maximum}</li>
      })
    }

    if (this.state.allEmp.length > 0) {
      //alert(this.state.allEmp);
      var employee = this.state.allEmp.map(function (employee) {
        return <li>{employee.minimum}-{employee.maximum}</li>
      })
    }
    if (this.state.certification.length > 0) {
      console.log(this.state.certification);
      var cer = this.state.certification.map(function (cer) {
        return <li>{cer}</li>
      })
    }
    if (this.state.firmTypes.length > 0) {
      console.log(this.state.firmTypes);
      var type = this.state.firmTypes.map(function (type) {
        return <li>{type}</li>
      })
    }
    if (this.state.companyIndustries.length > 0) {
      console.log(this.state.companyIndustries);
      var industry1 = this.state.companyIndustries.map(function (industry1) {
        return <li>{industry1.industry}</li>
      })
    }
    if (this.state.accolades.length > 0) {
      console.log(this.state.accolades);
      var acco = this.state.accolades.map(function (acco) {
        console.log(acco)
        return <li>{acco}</li>
      })
    }
    if (this.state.serviceArea.length > 0) {
      console.log(this.state.serviceArea);
      var service = this.state.serviceArea.map(function (service) {
        console.log(service);
        return <li> {service.service_area} </li>

      })
    }
    if (this.state.allServices.length > 0) {

      var myservice = this.state.allServices.map(function (myservice) {

        return (myservice.services.map(function (item) {
          return (
            <li>{item.service}</li>
          );
        })
        )
      })
    }

    console.log(this.state.sidebarData);
    if (this.state.sidebarData) {

      var sidebarpagedata = this.state.sidebarData.companies;
      var sidebarpagedata = this.state.sidebarData.companies.map((sidebarpagedata, index) => {
        //            var applied= 0;
        var images = 'images/logo.png';
        if (sidebarpagedata.applied == 0) {
          return <Col md={4} className="" id={"select" + index}>

            <div className="company-logo">
              <Thumbnail src={images} alt="242x200" >
                <h5 className="sidebar-cmpy" style={{ fontWeight: '600' }}>{sidebarpagedata.name} <span className="cal pull-right">{sidebarpagedata.website}</span></h5>
                <p className="text sidebar-p">{sidebarpagedata.year_founded}<span className="number pull-right">{sidebarpagedata.email_address}</span></p>
                <hr />
                <p>
                  <Link to="/Companydetail" className="text sidebar-list-more">More Details</Link>
                  &nbsp;
                   <Button type="button" className="default pull-right" onClick={() => this.addCompnay(sidebarpagedata.id, index)} >Apply Now</Button>
                </p>
              </Thumbnail>
            </div>
          </Col>
        }
        else {
          return <Col md={4} className="slct_div selected_class" id={"select" + index}>
            <div className="company-logo">
              <div className="overlay">
                <div className="text"><strong><i className="fa fa-check" style={{ fontSize: "80px", color: 'white' }}></i></strong></div>
              </div>
              <Thumbnail src="./images/logo.png" alt="242x200">
                <h5 className="sidebar-cmpy">{sidebarpagedata.name}<span className="cal pull-right">{sidebarpagedata.website}</span></h5>
                <p className="text sidebar-p">{sidebarpagedata.year_founded}<span className="number pull-right">{sidebarpagedata.email_address}</span></p>
                <hr />
                <p>
                  <Link to="/Companydetail" className="text sidebar-list-more">More Details</Link>
                  &nbsp;
                <Button type="button" className="pull-right" bsStyle="warning" onClick={() => this.addCompnay(sidebarpagedata.id, index)}>Applied</Button>
                </p>
              </Thumbnail>
            </div>
          </Col>
        }

      })
    }

    return (

      <div className="container-fluid">
        <div className="row">
          <Col md={12} sm={12}>
            <Col md={3}>
              <h2 className="text-center">Refine Search</h2>
              <input type="text" className="form-control empty" placeholder='&#61442;' style={{ position: 'absolute', width: '90%' }} />
              <hr />
              <section className="sidebar">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>

                <div class="collapse navbar-collapse" id="sidebar ">
                  <ul className="sidebar-menu tree" data-widget="tree" >
                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Service Area</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>{service}</li>
                        </ul>
                      </ul>
                    </li>


                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Service(s)</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>{myservice}</li>
                        </ul>
                      </ul>
                    </li>



                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Location</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                    </li>


                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Company Industry</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>
                            {industry1}
                          </li>
                        </ul>
                      </ul>
                    </li>

                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Company accolades</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>
                            {acco}
                          </li>
                        </ul>
                      </ul>
                    </li>

                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Company Revenue</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>
                            {measurement}
                          </li>
                        </ul>
                      </ul>
                    </li>
                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">company employee</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>
                            {employee}
                          </li>
                        </ul>
                      </ul>
                    </li>
                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Firm Types</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>
                            {type}
                          </li>
                        </ul>
                      </ul>
                    </li>
                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Company Stage</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>

                    </li>

                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Resource(s) need</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>

                    </li>

                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Certification need</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          <li>
                            {cer}
                          </li>
                        </ul>
                      </ul>
                    </li>
                  </ul>

                  <div style={{ marginTop: '15%' }}>
                    <Button type="button" bsStyle="" style={{ width: '38%', backgroundColor: '#4dac4c', color: 'white' }}>Apply</Button>
                    <Button type="button" bsStyle="" style={{ marginLeft: '19%', width: '38%', backgroundColor: '#1d4e56', color: 'white' }}>Reset</Button>
                  </div>
                </div>
              </section>
            </Col>

            <Col md={9}>
              <h2 className="text-center">Select Up to 5 Contact</h2>
              <p className="sidebartext text-center" >contact firms with one click </p>
              <hr className="contact" />
              <div className="row">
                {sidebarpagedata}

              </div>
              <div className="text-center" >
              </div>
            </Col>
          </Col>
          <div className="text-center" >
            <Button className="sidebar-contact" onClick={this.submit} bsStyle="" style={{ marginLeft: '26%', marginTop: '5%', width: '14%', borderRadius: '-15px', fontSize: '24px', color: 'white' }}>Contact Firms</Button>
          </div>

        </div>
      </div>

    );
  }
}
export default Sidebar;
