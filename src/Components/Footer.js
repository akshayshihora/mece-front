import React, { Component } from 'react';
import {Image} from 'react-bootstrap';
import SocialMediaIcons from 'react-social-media-icons';
import { InputGroup, InputGroupText, InputGroupAddon, Input } from 'reactstrap';
import Home from './Home';
const socialMediaIcons = [
	{
	url: 'https://linkedin.com',
	className: 'fab fa-linkedin-in footer-soc',
	},
	{
	url: 'https://twitter.com',
	className: 'fab fa-twitter footer-soc',
	},

];

class Footer extends Component {
	render() {

	return (
	
		<div style ={{ backgroundImage: "url(./images/World-Map-Free-PNG-Image.png)" }} className="row footer-div">
		<div className = "container">
		<div class="col-md-3 footer-left-div">
			<Image style={{height:'50px'}} className="logo" src="./images/2.jpg" />
			<p className="footer-left-div-p">Bld Mihail Kogalniceanu, <br /> nr 8, 7652 Bucharest, <br /> Texas</p>
		</div>
		<div class="col-md-8 footer-center-div">
		<div className="col-md-12">
			<div className="col-sm-2 col-xs-4 footer-menu">
			<a href = "#">Try it out</a> &nbsp;
			</div>
			<div className="col-sm-2 col-xs-3 footer-menu">
			<a href = "/Home">Demo</a>&nbsp;

			</div>
			<div className="col-sm-2 col-xs-3 footer-menu">
			<a href = "/About">About</a> &nbsp;
			</div>
			<div className="col-sm-1 col-xs-2 footer-menu">
			<a href = "/Blog">Blog</a> &nbsp;
			</div>
			<div className="col-sm-2 col-xs-4 footer-menu">
			<a href = "/Reg">Contact</a> &nbsp;
			</div>
			<div className="col-sm-3 col-xs-8 footer-menu">
			<a href = "#">Terms of services</a>
			</div>
		</div>

		<div className="subscribe-div">
		<p> Subscribe Us </p>
		<InputGroup className="footer-email-group">
			<Input className="inpt-text footer-email" placeholder="Email Address" style={{color:'white'}}/>
			<InputGroupAddon addonType="append" style={{float:'right', marginTop: '-39px',padding:'2px 1px'}}>
			<InputGroupText className='btn btn-warning' style={{fontSize:'16px',borderTop: 'solid 1px #ec9926'}}><i class="fa fa-angle-right" aria-hidden="true"></i></InputGroupText>

			</InputGroupAddon>
		</InputGroup>
		</div>

		</div>

		<div class="col-md-1 footer-right-div">
		<div className="text-center">
		<p style={{color:'white',fontWeight:'bold',fontSize:'15px'}}> Follow Us </p>
		<SocialMediaIcons
		icons={socialMediaIcons}
		iconSize={'1.3em'}
		iconColor={'#495056'}
		/>

		</div>

		</div>

		<div className='copyright-div text-center'>
		<p>Copyright 2017,MECE All Right Reserved </p>
		 

		</div>

		</div>

		</div>

	);
}
}

export default Footer;