import React from 'react';
import { Col, Image } from 'react-bootstrap';
import $ from 'jquery';
import ReactGA from 'react-ga';

class Blog extends React.Component {

  constructor(props) {
    super(props);
    console.log("pro", this.props);

    this.state = {
      detailsData: this.props.location.state.bloglistData,
    };

    ReactGA.initialize('UA-110570651-1');
    ReactGA.pageview(window.location.pathname);

  }

  getBlogdetailsData() {
    // $.ajax({
    //   url:'/blogDetail.json',
    //   dataType:'json',
    //   cache: false,
    //   success: function(data){
    //     this.setState({detailsData: data});
    //   }.bind(this),
    //   error: function(xhr, status, err){
    //     console.log(err);
    //   }
    // });
  }

  componentDidMount() {
    this.getBlogdetailsData();
  }
  render() {
    if (this.state.detailsData) {

      var maintitle = this.state.detailsData.title;
      var miandescription = this.state.detailsData.description;
      var detailImage = "/images/blog-list-image/" + this.state.detailsData.image;
      var blogDetailImage = <div className="text-center blog_detimg" style={{ backgroundImage: "url(" + detailImage + ")" }}> </div>

    }

    return (

      <div className="container">

        <div className="row">
          <Col md={12} sm={12}>
            <h1 className="cust_headtitle">
              {maintitle}
            </h1>
            {blogDetailImage}
            <p className="blogtext-p blogdet_text" dangerouslySetInnerHTML={{ __html: miandescription }}></p>

          </Col>
        </div>
      </div>
    );
  }
}
export default Blog;

