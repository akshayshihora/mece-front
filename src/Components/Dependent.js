import React, { Component } from 'react';
class DynamicDrop extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedView: 'Cheating'
    }
  }
  
  render() {
    const { selectedView } = this.state
    const VIEWS = [
      {
        name: 'Cheating', 
        minor: ['a', 'b'], 
        method: ['apple', 'orange']
      }, {
        name: 'Abductions', 
        minor: ['AB', 'BC', 'X'], 
        method: ['cat', 'dog']
      }, 
    ]

    const getMajorMethod = () => {
      const view = VIEWS.filter(({name}) => name === selectedView)[0]
      return (
        <div>
          <select>
            {view.minor.map(m => <option>{m}</option>)}
          </select>
          <select>
            {view.method.map(m => <option>{m}</option>)}
          </select>
        </div>
      )
    }
    return (
      <div>
      <h2>Depend Dropdown</h2>
        <select onChange={(e) => this.setState({selectedView: e.target.value})}>
          {VIEWS.map(({name}) => <option value={name}>{name}</option>)}
        </select>

        {getMajorMethod()}
      </div>
    )
  }
}

export default DynamicDrop;