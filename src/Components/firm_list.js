import React from 'react';
import { Button, Glyphicon, Col, Thumbnail, Grid, Row, Image, Img, Tab } from 'react-bootstrap';
import './reg.css';
import $ from 'jquery';
import { BrowserRouter as Router, Route, Link, browserHistory } from 'react-router-dom';
import Thankyou from './Thankyou';
import Pagination from "react-js-pagination";
import axios from 'axios';
import Select from "react-select";
import PropTypes from "prop-types";
import { withRouter } from "react-router";



var subservice_array = [];
var service_array = [];
var new_array = [];
var selected_filters = [];
var selected_subservices = [];
var all_service_selected = [];
var all_company_statge = [];
var all_certification = [];
var all_accolades = [];
var all_industry = [];
var all_revanue = [];
var max = [];
var min = [];
var selected_company_data = [];
var selected_company_data_id = [];
var cmp_data = [];
var all_initial_data = [];
var acco = [];
var indu = [];
var certific = [];
var stages = [];
var compare = [];




class firm_list extends React.Component {

  customStyles = {
    valueContainer: (provided, state) => ({
      ...provided,
      textOverflow: "unset",
      maxWidth: "90%",
      whiteSpace: "nowrap",
      overflow: "hidden",
      display: "initial"
    })
  };
  multiValueContainer = ({ selectProps, data }) => {
    const label = data.label;
    const allSelected = selectProps.value;
    const index = allSelected.findIndex(selected => selected.label === label);
    const isLastSelected = index === allSelected.length - 1;
    const labelSuffix = isLastSelected ? ` (${allSelected.length})` : ", ";
    const val = `${label}`;
    return val;
  };
  constructor(props) {
    super(props);
    console.log("all", this.props.location.data);
    var responses = [];


    if (typeof this.props.location.data === 'undefined') {


    }
    else {
      console.log("$vishal", this.props.location.data);
      responses = this.props.location.data['response'];
      console.log("responses", responses);

      selected_filters = this.props.location.data['services'];
      selected_subservices = this.props.location.data['subservices'];
      console.log("akshay", this.props.location.data['allsubservices']);
      var new_service_areas_list = this.props.location.data['allsubservices'];
      var new_service_areas_list_new = [];
      all_service_selected = [];
      $.each(new_service_areas_list, function (index, value) {
        if (value) {
          new_service_areas_list_new[index] = { 'service': value }
        }

      });
      all_service_selected.push(new_service_areas_list_new);
      responses.companies.map((sidebarpagedata, index) => {

        responses.companies[index]['applied'] = 0;
      })
    }

    this.state = {
      sidebarData: responses,
      getData: {},
      allMeasurement: [],
      allEmp: [],
      certification: [],
      firmTypes: [],
      companyIndustries: [],
      industries: [],
      accolades: [],
      certifications: [],
      serviceArea: [],
      allServices: [],
      selectedfirm: [],
      ServicesData: {},
      homedata: {},
      services: [],
      subservices: all_service_selected,
      service: [],
      //  location: [],
      service_areas: [],
      filter: [],
      stages: [],
      ranges: [],
      filters: [],
      "allIndustries": [],
      "rangesall": [],
      "companyAll": [],
      "getAllCompany": [],
      "accoladesAll": [],
      "allRanges": [],
      "allCertification": [],
      "servicesAll": [],
    }
    this.addCompnay = this.addCompnay.bind(this);
    this.submit = this.submit.bind(this);
    this.addselectservice = this.addselectservice.bind(this);
    this.serviceAreas = this.serviceAreas.bind(this);
    this.optionClicked = this.optionClicked.bind(this);
    this.Submit1 = this.Submit1.bind(this);
    this.myFunction = this.myFunction.bind(this);
    this.myFunction2 = this.myFunction2.bind(this);
    this.Resetsubmit = this.Resetsubmit.bind(this);
    this.companyStage = this.companyStage.bind(this);
    this.getIdustries = this.getIdustries.bind(this);
    this.companyStageClick = this.companyStageClick.bind(this);
    this.SubmitApply = this.SubmitApply.bind(this);
  }

  componentDidMount() {

    this.getRevanue();
    this.getemp();
    this.getcertification();
    this.getTypes();
    this.getIdustries();
    this.accolaess();
    this.serviceArea();
    this.companyStage();
    localStorage.getItem("response");
  }
  myFunction(service) {


    selected_filters = $.grep(selected_filters, function (value) {
      return value != service;
    });

    this.state.serviceArea.map((expservice, index) => {
      if (expservice.service_area == service) {
        var last_services = expservice.services;

        last_services.map((ser) => {

          selected_subservices = $.grep(selected_subservices, function (value) {
            return value != ser.service;
          });
          console.log("vishal", all_service_selected);

          var count1 = 0;
          $.each(all_service_selected, function (index, value) {
            all_service_selected[count1] = $.grep(value, function (value1) {
              return value1.service != ser.service;
            });
            count1++;
          });



        })
        this.setState({ services: selected_filters })
        this.setState({ subservices: all_service_selected });
        if (selected_filters.length == 0) {
          selected_filters = [];
          selected_subservices = [];
          all_service_selected = [];
          this.setState({ subservices: [] });
          this.setState({ services: [] })
        }
      }
      //expservice.service_area services
    })

    if (selected_filters.length == 0) {

    }
    var i = 0;

  }

  myFunction2(service3) {

    selected_subservices = $.grep(selected_subservices, function (value) {
      return value != service3;
    });
    var i = 0;

    this.setState({ filter: i });

    i++;
    console.log("check", selected_subservices);
  }

  myFunction3(company) {

    all_company_statge = $.grep(all_company_statge, function (value) {
      return value != company;
    });
    var i = 0;

    this.setState({ filter: i });

    i++;
    console.log("check", all_company_statge);
  }

  myFunction4(certifi) {

    all_certification = $.grep(all_certification, function (value) {
      return value != certifi;
    });
    var i = 0;

    this.setState({ filter: i });

    i++;
    console.log("check", all_certification);

    if($.inArray( certifi, all_certification ))
    {
    
     certific = [];                       

     certific.push(all_certification);

    }
    else
    {
      alert('not exist');
    }
  }


  accoloselect(accoselect,index) {

    all_accolades = $.grep(all_accolades, function (value) {
      return value != accoselect;
    });
    var i = 0;

    this.setState({ filter: i });

    i++;
    console.log("check", all_accolades);
    console.log("check2",accoselect); 

    acco = [] 
    var a = accoselect
    acco.pop(a);
    acco.push(all_accolades);
  
  //  acco.push(all_accolades);

    //  if($.inArray( accoselect, all_accolades ))
    //  {
    //     acco = [];   
    
    //     acco.push(all_accolades);
    //  }
    //  else
    //  {
    //    alert('not exist');
    //  }
  
   }
  induselect(indusselect) {
    all_industry = $.grep(all_industry, function (value) {
      return value != indusselect;
    });
    var i = 0;

    this.setState({ filter: i });

    i++;
    console.log("check", all_industry);
    console.log("check2",indusselect);
    if($.inArray( indusselect, all_industry ))
    {
       var b = indusselect
       indu.pop(b);
       indu = [];  
      indu.push(all_industry);

    }
    else
    {
      alert('not exist');
    }
  }
  rengselect(rengselect) {

    all_revanue = $.grep(all_revanue, function (value) {
      return value != rengselect;
    });
    var i = 0;

    this.setState({ filter: i });

    i++;
    console.log("value1", all_revanue);
    console.log("value", rengselect)



  }


  getSidebarData() {
    $.ajax({
      url: '/sidebar.json',
      dataType: 'json',
      cache: false,
      success: function (data) {
        //        this.setState({sidebarData: data});
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(err);
        //alert(err);
      }
    });
  }

  getRevanue() {
    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/revenue-ranges',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ allRanges: data.ranges });

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });

  }


  getemp() {


    var getemp = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/employee-ranges";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {

        'Access-Control-Allow-Origin': '*',


      },
      url: getemp,
      success: function (data) {
        data = '[{"minimum":1,"maximum":10},{"minimum":11,"maximum":50},{"minimum":51,"maximum":200},{"minimum":201,"maximum":500},{"minimum":501,"maximum":1000},{"minimum":1001,"maximum":5000}, {"minimum":5001,"maximum": 10000}]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ allEmp: myObject });

      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }

    });
  }

  serviceArea() {
    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ serviceArea: data['service_areas'] });
        console.log("allgood", this.state.serviceArea);

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });

  }

  getcertification() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/certifications',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ allCertification: data.certifications });

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });
  }

  getTypes() {
    var getTypes = "https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/firm-types";
    $.ajax({
      type: 'GET',
      mode: 'no-cors',
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      url: getTypes,
      success: function (data) {
        data = '["Educational Institution", "Government Agency","Non-profit","Partnership", "Privately Held","Public Company","Self-Employed","Sole Proprietorship"]';
        var myObject = JSON.parse(data);
        //  var allMeasurement
        this.setState({ firmTypes: myObject });

      }.bind(this),

      error: function (xhr, status, err) {
        //alert("error block");
        console.log(err);
      }
    });
  }
  getIdustries() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/industries',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ allIndustries: data.industries });
        console.log("akki", this.state.allIndustries);

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });
  }
  accolaess() {
    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/accolades',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ accoladesAll: data.accolades });

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });

  }
  companyStage() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/company-stages',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ companyAll: data.stages });
        console.log("get123", this.state.companyAll)

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });
  }
  addCompnay(id, index) {

    const final_companies = this.state.sidebarData;
    if (final_companies['companies'][index]['applied'] == 0) {
      let s = 0;
      final_companies['companies'].forEach(function (e) {
        console.log(e['applied']);
        if (e['applied'] == 1) {
          s++;
        }
      });
      if (s < 5) {
        console.log(s);
        final_companies['companies'][index]['applied'] = 1;
        this.setState({ sidebarData: final_companies });
        new_array.push(id)
        this.setState({ selectedfirm: new_array });
      }
      else {
        alert("You can selecte upto 5 firms only.")
      }

    }
    else {
      final_companies['companies'][index]['applied'] = 0;
      this.setState({ sidebarData: final_companies });
      var index = new_array.indexOf(id);
      if (index !== -1) new_array.splice(index, 1);
      this.setState({ selectedfirm: new_array });
    }
  }
  submit() {
    //    alert("submitted");
    $('.spanner').show();
    console.log(this.state.selectedfirm);
    if (this.state.selectedfirm.length > 0) {
      this.props.history.push({
        pathname: '/firm_enquiry',
        data: this.state.selectedfirm // your data array of objects
      })
      $('.spanner').hide();
    }
    else {
      alert("Please select firms first");
      $('.spanner').hide();
    }
  }
  addselectservice(event) {
    console.log(event);

    if (!service_array.includes(event)) {
      service_array.push(event);
    }
    else {
      service_array.splice(service_array.indexOf(event), 1);
    }
    this.setState({ selectservice: service_array })

    if (service_array.length > 0 && this.state.addselectsubservice.length > 0 && typeof (this.state.location[0]) != "undefined") {
      this.setState({ bttondisabled: false });
    }
    else {
      this.setState({ bttondisabled: true });
    }
  }
  addselectsubservice(event) {
    console.log(event);

    if (!subservice_array.includes(event)) {
      subservice_array.push(event);
    }
    else {
      subservice_array.splice(subservice_array.indexOf(event), 1);
    }

    this.setState({ addselectsubservice: subservice_array })

    if (subservice_array.length > 0 && this.state.selectservice.length > 0 && typeof (this.state.location[0]) != "undefined") {
      this.setState({ bttondisabled: false });
    }
    else {
      this.setState({ bttondisabled: true });
    }
  }


  servicearea() {
    window.onmousemove = function () {

      var anchors = document.getElementsByClassName('servicebox');

      for (var i = 0; i < anchors.length; i++) {
        var anchor = anchors[i];
        anchor.onclick = function () {

          var id = this.id;
          var str = id.slice(3, 5);
          str = parseInt(str) - 1;
          //alert(str);
          var element = document.getElementById(id);
          element.classList.toggle("slct_class");
          element.classList.toggle("unslct_class");
          var slct_class = document.getElementsByClassName('slct_class');
          if (slct_class.length > 0) {
            document.getElementsByClassName('subservice-area')[0].style.display = 'block';
          }
          else {
            document.getElementsByClassName('subservice-area')[0].style.display = 'none';
          }

          if (element.classList.contains('slct_class')) {
            //                    alert('id-tab-'+str);
            document.getElementById('id-tab-' + str).style.display = 'block';
            // document.getElementsByClassName('nav-tabs').style.display = 'block';
            document.getElementById('id-pane-' + str).style.display = 'block';
            document.getElementById("id-pane-" + str).setAttribute(
              "style", "color:#4dac4c;font-size:15px");
            document.getElementById("id-tab-" + str).setAttribute(
              "style", "color:#4dac4c;font-size:15px");
            document.getElementById("myBtn").style.visibility = 'visible';
            // document.getElementById("myBtn").style.display='none';
            // var ul = document.getElementsByClassName("nav-tabs");
            // for (var i = 0; i < ul[0].childNodes.length; i++) {
            //     ul[0].childNodes[i].classList.remove("active");
            // }
            // var ul = document.getElementsByClassName("tab-pane");
            // for (var i = 0; i < ul.length; i++) {
            //     ul[i].classList.remove("active");
            //     ul[i].classList.remove("in");
            // }
            // document.getElementById('id-tab-'+str).parentElement.classList.add("active");
            // document.getElementById('id-pane-'+str).classList.add("active");
            // document.getElementById('id-pane-'+str).classList.add("in");
            $("#id-tab-" + str)[0].click();
          }
          else {

            document.getElementById('id-tab-' + str).style.display = 'none';
            document.getElementById('id-pane-' + str).style.display = 'none';
            var clicked_pen = $(".tab-content .tab-pane:visible");
            console.log(clicked_pen.length);
            if (clicked_pen.length == 0) {
              if ($(".nav.nav-tabs li:visible").children('a').length > 0) {
                $(".nav.nav-tabs li:visible").children('a')[0].click();
              }
            }
            document.getElementById("myBtn").style.visibility = 'hidden';
            var el = $("#id-pane-" + str + " .boxsubservice");
            el.removeClass('slct_class1');

            //   document.getElementsByClassName('nav-tabs').style.display = 'none';
          }
        }
      }

      var anchors1 = document.getElementsByClassName('boxsubservice');
      for (var i = 0; i < anchors1.length; i++) {
        var anchor = anchors1[i];
        anchor.onclick = function () {
          var slct_class = document.getElementsByClassName('slct_class1');
          var id = this.id;
          var element = document.getElementById(id);
          element.classList.toggle("slct_class1");
          element.classList.toggle("unslct_class");

        }
      }
    }
  }
  optionClicked(optionsList, service_area_selected) {
    if (!selected_filters.includes(service_area_selected)) {
      console.log("filters", selected_filters);

      selected_filters.push(service_area_selected)
      console.log("new", optionsList);

      all_service_selected.push(optionsList);
      this.setState({ subservices: all_service_selected });
      console.log("first", this.state.subservices);
      console.log("first1", all_service_selected)

      // selected_filters.push(service_area_selected)


      // this.setState({ filter: indu });
      // console.log("indus",indu);

    }
  }
  subserviceClicked(optionsList, subservice) {
    if (!selected_subservices.includes(optionsList)) {
      selected_subservices.push(optionsList);
      console.log("new1", optionsList);
      this.setState({ services: selected_subservices });
      console.log("firstt", this.state.services);
      console.log("firstt1", selected_subservices)

    }
  }

  companyStageClick(company_stage) {

    if (!all_company_statge.includes(company_stage)) {
      console.log("akshay123", all_company_statge);

      all_company_statge.push(company_stage)
      console.log("akshay1234", company_stage);

      this.setState({ stages: all_company_statge });
      console.log("ram", this.state.stages);
      console.log("ram1", all_company_statge);


      stages.push({ 'company_stage': company_stage })
      this.setState({ filter: stages });
      console.log("stages", stages);
    }
  }

  certificationClick(certi) {

    if (!all_certification.includes(certi)) {
      console.log("akshay123", all_certification);

      all_certification.push(certi)
      console.log("akshay1234", certi);

      this.setState({ certifications: all_certification });

      console.log("ram1", all_certification);

      //  var abc = this.state.sidebarData.companies.length;
      //  console.log("akshay",abc);


      //  this.state.sidebarData.companies.map((certi1,index) => {

      //    console.log("akshay1",indus);

      //    console.log("akshay11",indus1.industries_served);

      //   if(certi1.certifications == certi){
      //     alert("hii");

      //   } 
      //   else{
      //     alert(index);
      //     this.state.sidebarData.companies.splice(index, 1);
      //   }
      //  })

      certific.push(certi)
      this.setState({ filter: certific });
      console.log("certification450", certific);


    }
  }

  accoloadesClick(accola) {
  

    if (!all_accolades.includes(accola)) {
      console.log("akshay123", all_accolades);

      all_accolades.push(accola)
      console.log("akshay1234", accola);

      this.setState({ accolades: all_accolades });

      console.log("mayur", all_accolades);

      //  var pqr = this.state.sidebarData.companies.length;
      //  console.log("akshay",pqr);


      //  this.state.sidebarData.companies.map((accola1,index) => {



      //   if(accola1.accolades == accola){


      //   } 
      //   else{


      //     this.state.sidebarData.companies.splice(index,1);
      //   }
      //  })


      //  all_accolades.push(accola)
     
      acco.push(accola)

      //   acco['accolades'] = accola;

      this.setState({ filter: acco });
      console.log("shihora", acco);
    }
  

  }


  industryClick(indus) {

    if (!all_industry.includes(indus)) {
      console.log("ramm", all_industry);

      all_industry.push(indus)
      console.log("ramm1", indus);

      this.setState({ industries: all_industry });

      // console.log("ramm11",all_industry);

      // var mno = this.state.sidebarData.companies.length;
      // console.log("akshay",mno);


      //  this.state.sidebarData.companies.map( (indus1,index) => {

      //     console.log("akshay1",indus);

      //    console.log("akshay11",indus1.industries_served);

      //   if(indus1.industries_served == indus){
      //      alert("hii");

      //   } 
      //   else{
      //      alert(index);
      //     this.state.sidebarData.companies.splice(index, 1);
      //   }
      //  })


      indu.push(indus)


      this.setState({ filter: indu });
      console.log("indus", indu);

    }

  }


  revanueClick(minimum, maximum) {
    //console.log("akshay");

    if (!all_revanue.includes(minimum, maximum)) {
      //   console.log("ramm", all_revanue);

      all_revanue.push(minimum, maximum)
      // console.log("ramm1", revanu);

      this.setState({ ranges: all_revanue });

      console.log("ramm11", all_revanue);


    }

  }




  initMap() {
    var map = new window.google.maps.Map(document.getElementById('automap'), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13
    });

    var input = document.getElementById('pac-input');

    var autocomplete = new window.google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(
      ['address_components', 'geometry', 'icon', 'name']);

    var infowindow = new window.google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new window.google.maps.Marker({
      map: map,
      anchorPoint: new window.google.maps.Point(0, -29)
    });
    autocomplete.addListener('place_changed', (event) => {
      this.test(autocomplete.getPlace());
    });
    autocomplete.addListener('place_changed', function () {
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindowContent.children['place-icon'].src = place.icon;
      infowindowContent.children['place-name'].textContent = place.name;
      infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    });
  }
  Submit1() {

    $('.spanner').show();
    $.ajax({
      type: 'POST',
      mode: 'no-cors',
      headers: {
        'Content-Type': "application/json",
        'Access-Control-Allow-Origin': '*',
      },
      data: JSON.stringify({ "service_areas": selected_filters, "services": selected_subservices }),
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/mece/api/v1/search',
      success: function (response) {
        response.companies.map((sidebarpagedata, index) => {
          response.companies[index]['applied'] = 0;
        })
        //  console.log("this",this.state.sidebarData)
        console.log("akshayres123", response.companies);

        response.companies.map((sidebarpagedata, index) => {
          console.log("akshayres", response.companies);

          if (indu.length > 0) {

            var flag = 0;
            sidebarpagedata.industries_served.map((industries_serve) => {

              if (indu.includes(industries_serve)) {
                flag = 1;

                return;
              }
              else {
                //alert(index);
                flag = 0;

              }
            })
            if (flag == 0) {
              console.log("flag", response.companies);
              alert("index" + index);
              response.companies.splice(index, 1);

            }

          }



          if (acco.length > 0) {

            var flag = 0;
            sidebarpagedata.accolades.map((accoloadies) => {

              if (indu.includes(accoloadies)) {
                flag = 1;

                return;
              }
              else {

                flag = 0;

              }
            })
            if (flag == 0) {
              console.log("flag", response.companies);
              response.companies.splice(index, 1);

            }

          }



          //   if(acco.length > 0){
          //     if(sidebarpagedata.accolades == acco){

          //       console.log("match",acco);
          //    } 
          //    else{
          //     // console.log("match1",acco);

          //      response.companies.splice(index, 1);
          //    }
          //   }
          //    if(certific.length > 0){
          //    if(sidebarpagedata.certifications == certific){

          //     console.log("match",certific);
          //  } 
          //  else {
          //  //  console.log("match1",certific);
          //    response.companies.splice(index, 1);
          //  }
          // }

          //    if(sidebarpagedata.accolades == stages){

          //     console.log("match",stages);
          //  } 
          //  else{
          //    console.log("match1",stages);
          //    response.companies.splice(index, 1);
          //  }


        })

        this.setState({ sidebarData: response });
        $('.spanner').hide();
        // this.props.onUpdate(response.companies);
      }.bind(this),

      error: function (xhr, status, err) {
        //   alert("akshay");
        console.log(xhr.status);
        if (xhr.status == 503)

          alert("Please try again. No match found.");
        this.props.history.push({
          pathname: '/firm_list',
        })

        $('.spanner').hide();
      }.bind(this),
    });

  }

  SubmitApply() {

console.log("check3", all_accolades);
//   alert(all_accolades);
//console.log("selectedcompany",indu);

console.log("checkselectedcompany",acco);


//console.log("selectedcompany",certific);

    selected_company_data = [];
    selected_company_data_id = [];
    cmp_data = [];
    // sidebarpagedata = all_initial_data;

    console.log("all_initial_data = ",all_initial_data[0]);

    all_initial_data[0].companies.map((sidebarpagedata, index) => {
      all_initial_data[0].companies[index]['applied'] = 0;
    })

    this.setState({ sidebarData: all_initial_data[0] });
    setTimeout(function(){ 
    this.state.sidebarData.companies.map((sidebarpagedata, index) => {
      this.state.sidebarData.companies[index]['applied'] = 0;
      console.log("submit", sidebarpagedata);
    })
    var comp_index = 0;
    this.state.sidebarData.companies.map((sidebarpagedata, index) => {

      
      console.log("akshay2 selected_filters result ", selected_filters.length);
      if (selected_filters.length > 0) {

      

            for (var i = 0; i < sidebarpagedata.service_areas.length; i++) {
            
                  for (var j = 0; j < selected_filters.length; j++) {
              
                     
                    if (selected_filters[j] == sidebarpagedata.service_areas[i]) {
                
                     console.log("selected_company_data_id",selected_company_data_id);
                     console.log("selected_company_data_id selected ",sidebarpagedata.id);
                     if(selected_company_data_id.indexOf(sidebarpagedata.id) > -1)
                     {     
                       // do nothing          
                     }
                     else
                     {
                      selected_company_data_id.push(sidebarpagedata.id);
                      selected_company_data.push(sidebarpagedata);
                      
                     }
                      continue;
                    }
                  }
                }


      }

      if (selected_subservices.length > 0) {

          for (var i = 0; i < sidebarpagedata.services.length; i++) {
            //    alert( "length = "+indu.length);
                for (var j = 0; j < selected_subservices.length; j++) {
                  // do logic here
              //    alert("indu before = " + indu[j] + " **indus before = " + sidebarpagedata.industries_served[i]);
                   
                  if (selected_subservices[j] == sidebarpagedata.services[i]) {
               //     alert("indu = " + indu[j] + " **indus = " + sidebarpagedata.industries_served[i]);
                   console.log("selected_company_data_id",selected_company_data_id);
                   console.log("selected_company_data_id selected ",sidebarpagedata.id);

                   if(selected_company_data_id.indexOf(sidebarpagedata.id) > -1)
                   {     
                     // do nothing          
                   }
                   else
                   {
                    selected_company_data_id.push(sidebarpagedata.id);
                    selected_company_data.push(sidebarpagedata);
                   }
                    continue;
                  }
                }
              }


      }


      if (indu.length > 0) {
      
        //  console.log("allcompany",sidebarpagedata); // all company
        //  console.log("all",sidebarpagedata.industries_served); // all service
        console.log("indusss",indu); // all selected service
        for (var i = 0; i < sidebarpagedata.industries_served.length; i++) {
      //    alert( "length = "+indu.length);
          for (var j = 0; j < indu.length; j++) {
            // do logic here
        //    alert("indu before = " + indu[j] + " **indus before = " + sidebarpagedata.industries_served[i]);
             
            if (indu[j] == sidebarpagedata.industries_served[i]) {
         //     alert("indu = " + indu[j] + " **indus = " + sidebarpagedata.industries_served[i]);
             console.log("selected_company_data_id",selected_company_data_id);
             console.log("selected_company_data_id selected ",sidebarpagedata.id);
             if(selected_company_data_id.indexOf(sidebarpagedata.id) > -1)
             {     
               // do nothing          
             }
             else
             {
              selected_company_data_id.push(sidebarpagedata.id);
              selected_company_data.push(sidebarpagedata);
             }
              continue;
            }
          }
        }
      }

      if (acco.length > 0) {
       
        for (var m = 0; m < sidebarpagedata.accolades.length; m++) {
          for (var n = 0; n < acco.length; n++) {
            if (acco[n] == sidebarpagedata.accolades[m]) {
            
             console.log("selected_company_data_id",selected_company_data_id);
             console.log("selected_company_data_id selected ",sidebarpagedata.id);
             if(selected_company_data_id.indexOf(sidebarpagedata.id) > -1)
             {    
             
               // do nothing          
             }
             else
             {
              selected_company_data_id.push(sidebarpagedata.id);
              selected_company_data.push(sidebarpagedata);

        
             }
              continue;
            }
          }
        }
      }


      if (certific.length > 0) {
       
        for (var p = 0; p < sidebarpagedata.certifications.length; p++) {
          for (var q = 0; q < certific.length; q++) {
            if (certific[q] == sidebarpagedata.certifications[p]) {
             console.log("selected_company_data_id",selected_company_data_id);
             console.log("selected_company_data_id selected ",sidebarpagedata.id);
             if(selected_company_data_id.indexOf(sidebarpagedata.id) > -1)
             {     
               // do nothing          
             }
             else
             {
              selected_company_data_id.push(sidebarpagedata.id);
              selected_company_data.push(sidebarpagedata);
             }
              continue;
            }
          }
        }
      }


    })
   console.log("selectedcompany", selected_company_data); // all filetered company object
  //console.log("selectedcompany320",cmp_data);
      cmp_data['companies'] = selected_company_data;
      cmp_data.map((sidebarpagedata, index) => {
      cmp_data[index]['applied'] = 0;
    })
    this.setState({ sidebarData: cmp_data });
 
  }.bind(this), 1000);
}
  Resetsubmit() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/company/all',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (response) {

        acco = [];
        all_accolades = [];
        all_certification = [];
        certific = [];
        all_industry = [];
        indu = [];
        all_company_statge = [];
        stages = [];



        selected_filters = [];
        selected_subservices = [];
        all_service_selected = [];
        this.setState({ service_areas: [] });
        this.setState({ subservices: [] });
        this.setState({ selected_subservices: [] });
        //console.log("reset data "+response);
        all_initial_data.push(response);
        console.log("all_initial_data 2  ", response);
        response.companies.map((sidebarpagedata, index) => {
          response.companies[index]['applied'] = 0;
          
         // all_initial_data.push(sidebarpagedata);
        })
        
        // console.log("reset data ", {response});
        this.setState({ sidebarData: response });
        
          //  localStorage.setItem('myAllCompanyData', {response});
           
        $('.spanner').hide();
      }.bind(this),
      error: function (xhr, status, err) {

      }.bind(this),
    });
  }

  serviceAreas() {
    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
        this.setState({ serviceArea: data['service_areas'] });

      }.bind(this),
      error: function (xhr, status, err) {

      }
    });

  }

  render() {
    const {
      CustomOption,
      multiValueContainer,
      customStyles,
      ValueContainer
    } = this;

    if (this.state.homedata.servicearea) {
      var servicearea = this.state.homedata.servicearea;
      var services = this.state.homedata.services;
    }
    if (this.state.serviceArea.length > 0) {

    }

    if (this.state.ServicesData.exp) {
      var j = 1;
      var expservice = this.state.serviceArea.map((expservice, index) => {

        return <div className="col-md-3 col-sm-6">
          <div id={"box" + j} onClick={() => this.addselectservice(expservice.service_area)} className={"box" + j++ + " servicebox unslct_class"}>
            <div className="overlay-explore">
              <strong><i className="fa fa-check" style={{ fontSize: "50px", color: 'black' }}></i></strong>
            </div>
            <p className="box-p1">{expservice.service_area}</p>
          </div>
        </div>
      })
      var i = 0;
      var myservice = this.state.serviceArea.map((myservice, index) => {

        return (<Tab eventKey={index} title={myservice.service_area}>
          <div className="col-md-12 sales-content">
            {myservice.services.map((item) => {

              return <div className="col-md-3" onClick={() => this.addselectsubservice(item.service)}>
                <div id={"boxsubservice" + i++} className="boxsubservice unslct_class">
                  <div className="overlay-explore">
                    <div className="text"><strong><i className="fa fa-check" style={{ fontSize: "50px", color: 'black' }}></i></strong></div>
                  </div>
                  <p className="box-subservice-p1">{item.service}</p>
                </div>
              </div>
            })}
          </div>
        </Tab>
        )

      })
    }

    // all ranges display

    // if (this.state.allRanges.length > 0) {
    //   console.log(this.state.allRanges);
    //   var measurement = this.state.allRanges.map(function (measurement) {
    //     return <li>{measurement.minimum}-{measurement.maximum}</li>
    //   })
    // }

    if (this.state.allEmp.length > 0) {
      //alert(this.state.allEmp);
      var employee = this.state.allEmp.map(function (employee) {
        return <li>{employee.minimum}-{employee.maximum}</li>
      })
    }

    // this is certification
    // if (this.state.allCertification.length > 0) {
    //   console.log(this.state.allCertification);
    //   var cer = this.state.allCertification.map(function (cer) {
    //     return <li>{cer}</li>
    //   })
    // }

    if (this.state.firmTypes.length > 0) {
      console.log(this.state.firmTypes);
      var type = this.state.firmTypes.map(function (type) {
        return <li>{type}</li>
      })
    }
    //this is all industrys

    // if (this.state.allIndustries.length > 0) {
    //   console.log(this.state.allIndustries);
    //   var industry1 = this.state.allIndustries.map(function (industry1) {
    //     return <li>{industry1.industry}</li>
    //   })
    // }
    if (this.state.accoladesAll.length > 0) {
      console.log(this.state.accoladesAll);
      var acco = this.state.accoladesAll.map(function (acco) {
        console.log(acco)
        return <li>{acco}</li>
      })
    }

    // this is company stages

    // if (this.state.companyAll.length > 0) {
    //   console.log(this.state.companyAll);
    //   var reng = this.state.companyAll.map(function (reng) {
    //     console.log(reng)
    //     return <li>{reng}</li>
    //   })
    // }

    // end



    if (this.state.serviceArea.length > 0) {
      console.log(this.state.serviceArea);
      var service = this.state.serviceArea.map(function (service) {

        return <li> {service.service_area} </li>

      })
    }
    if (this.state.allServices.length > 0) {

      var myservice = this.state.allServices.map(function (myservice) {

        return (myservice.services.map(function (item) {
          return (
            <li>{item.service}</li>
          );
        })
        )
      })
    }
    console.log(this.state.sidebarData);
    if (this.state.sidebarData) {

      var sidebarpagedata = this.state.sidebarData.companies;
      if (this.state.sidebarData.companies != undefined) {

        var sidebarpagedata = this.state.sidebarData.companies.map((sidebarpagedata, index) => {

          //  var applied= 0;
          var images = 'images/logo.png';
          console.log("akkku1", sidebarpagedata.applied);
          if (sidebarpagedata.applied == 0) {
            return <Col md={4} className="" id={"select" + index}>

              <div className="company-logo">
                <Thumbnail >
                  <img src={sidebarpagedata.company_logo_url} />
                  <h5 className="sidebar-cmpy" style={{ fontWeight: '600' }}>{sidebarpagedata.name} <span className="cal pull-right">{sidebarpagedata.website}</span></h5>
                  <p className="text sidebar-p">{sidebarpagedata.year_founded}<span className="number pull-right">{sidebarpagedata.email_address}</span></p>
                  <hr />
                  <p>
                    <Link to={{ pathname: '/Companydetail', state: { 'selected': sidebarpagedata, 'companies': this.state.sidebarData.companies, 'selected_service': selected_filters, 'selected_service_area': selected_subservices, 'selcted_sub': all_service_selected } }} className="text sidebar-list-more" >

                      More Details</Link>
                    &nbsp;
               <Button type="button" className="default pull-right" onClick={() => this.addCompnay(sidebarpagedata.id, index)} >Select</Button>
                  </p>
                </Thumbnail>
              </div>
            </Col>
          }
          else {
            return <Col md={4} className="slct_div selected_class" id={"select" + index}>
              <div className="company-logo">
                <div className="overlay">
                  <div className="text"><strong><i className="fa fa-check" style={{ fontSize: "80px", color: 'white' }}></i></strong></div>
                </div>
                <Thumbnail >
                  <img src={sidebarpagedata.company_logo_url} alt="242x200" />
                  <h5 className="sidebar-cmpy">{sidebarpagedata.name}<span className="cal pull-right">{sidebarpagedata.website}</span></h5>
                  <p className="text sidebar-p">{sidebarpagedata.year_founded}<span className="number pull-right">{sidebarpagedata.email_address}</span></p>
                  <hr />
                  <p>
                    <Link to="/Companydetail" className="text sidebar-list-more">More Details</Link>
                    &nbsp;
            <Button type="button" className="pull-right" bsStyle="warning" onClick={() => this.addCompnay(sidebarpagedata.id, index)}>select</Button>
                  </p>
                </Thumbnail>
              </div>
            </Col>
          }

        })
      }
      else {

      }

    }

    return (

      <div className="container-fluid">
        <div className="row">
          <Col md={12} sm={12}>
            <Col md={3}>



              <div className="cust_filter_items" id="selected_filters">



                {selected_filters.map(service => (
                  console.log("value2", service),
                  <span>{service}<i class="fa fa-times" aria-hidden="true" onClick={() => this.myFunction(service)}></i></span>

                )
                )
                }

              </div>
              <div className="cust_filter_items" >


                {selected_subservices.map(service3 => (

                  <span>{service3}<i class="fa fa-times" aria-hidden="true" onClick={() => this.myFunction2(service3)}></i></span>

                )
                )
                }
              </div>


              {/* <div className="cust_filter_items" >

             
                {all_company_statge.map(company => (
                   console.log("value",company),
                  <span>{company}<i class="fa fa-times" aria-hidden="true" onClick={() => this.myFunction3(company)}></i></span>
                )
                )
                }
              </div>

              <div className="cust_filter_items" >

              {all_certification.map(certifi => (
                   console.log("value",certifi),
                  <span>{certifi}<i class="fa fa-times" aria-hidden="true" onClick={() => this.myFunction4(certifi)}></i></span>
                )
                )
                }
              </div> */}







              <div className="cust_filter_items" >

                {all_certification.map(certifi => (
                  console.log("value", certifi),
                  <span>{certifi}<i class="fa fa-times" aria-hidden="true" onClick={() => this.myFunction4(certifi)}></i></span>
                )
                )
                }


                {all_company_statge.map(company => (
                  console.log("value", company),
                  <span>{company}<i class="fa fa-times" aria-hidden="true" onClick={() => this.myFunction3(company)}></i></span>
                )
                )
                }
                {all_accolades.map(accoselect => (
                  console.log("value", accoselect),
                  <span>{accoselect}<i class="fa fa-times" aria-hidden="true" onClick={() => this.accoloselect(accoselect)}></i></span>
                )
                )
                }

                {all_industry.map(indusselect => (
                  console.log("value", indusselect),
                  <span>{indusselect}<i class="fa fa-times" aria-hidden="true" onClick={() => this.induselect(indusselect)}></i></span>
                )
                )
                }

              {/* {all_revanue.map(rengselect => (
                 //  console.log("value",rengselect),
                  <span> {rengselect}<i class="fa fa-times" aria-hidden="true" onClick={() => this.rengselect(rengselect)}></i></span>
                )
                )
                }  */}

              </div>
              <section className="sidebar">
                <div class="navbar-header">

                </div>

                <div class="navbar-collapse" id="sidebar ">
                  <ul className="sidebar-menu tree" data-widget="tree" >
                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Service Area</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }} >
                          {/* <li>{options}</li> */}
                          {console.log("new4", this.state.serviceArea)}

                          {this.state.serviceArea.map(service => (
                            (!selected_filters.includes(service.service_area)) ? (
                              <li onClick={() => this.optionClicked(service.services, service.service_area)} style={{ cursor: 'pointer' }}>{service.service_area}</li>
                            ) : (
                                <li onClick={() => this.optionClicked(service.services, service.service_area)}>
                                  <i class="fa fa-check" aria-hidden="true"></i>
                                  {service.service_area}</li>
                              )
                          )
                          )
                          }

                        </ul>
                      </ul>
                    </li>


                    <li className="treeview">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Service(s)</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '10%' }}>
                          {console.log("length", this.state.subservices.length)}
                          {this.state.subservices.map(service1 => (
                            service1.map(service => (
                              (!selected_subservices.includes(service.service)) ? (
                                <li onClick={() => this.subserviceClicked(service.service, service.service)} style={{ cursor: 'pointer' }}>{service.service}</li>
                              ) : (
                                  <li onClick={() => this.subserviceClicked(service.service, service.service)}>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {service.service}</li>
                                )
                            )
                            )))
                          }

                        </ul>
                      </ul>
                    </li>



                    <li className="treeview">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Location</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                    </li>


                    <li className="treeview cust_inner_li">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Company Industry</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '8%' }}>
                          <li>

                            {this.state.allIndustries.map(service7 => (
                              (!all_industry.includes(service7.industry)) ? (
                                <li onClick={() => this.industryClick(service7.industry)} style={{ cursor: 'pointer' }}>{service7.industry}</li>
                              ) : (
                                  <li onClick={() => this.industryClick(service7.industry)}>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {service7.industry}</li>
                                )
                            )
                            )
                            }


                            {/* {industry1} */}
                          </li>
                        </ul>
                      </ul>
                    </li>

                    <li className="treeview cust_inner_li">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Company accolades</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '8%' }}>
                          <li>
                            {this.state.accoladesAll.map(service6 => (
                              (!all_accolades.includes(service6)) ? (
                                <li onClick={() => this.accoloadesClick(service6)} style={{ cursor: 'pointer' }}>{service6}</li>
                              ) : (
                                  <li onClick={() => this.accoloadesClick(service6)}>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {service6}</li>
                                )
                            )
                            )
                            }

                            {/* {acco} */}
                          </li>
                        </ul>
                      </ul>
                    </li>


                    <li className="treeview cust_inner_li">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Company Revenue</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '8%' }}>
                          <li>

                            {this.state.allRanges.map(service8 => (


                              (!all_revanue.includes(service8.minimum, service8.maximum)) ? (
                                <li onClick={() => this.revanueClick(service8.minimum, service8.maximum)} style={{ cursor: 'pointer' }}>{service8.minimum}-{service8.maximum}</li>
                              ) : (
                                  <li onClick={() => this.revanueClick(service8.minimum, service8.maximum)}>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {service8.minimum}-{service8.maximum}</li>
                                )
                            )
                            )
                            }

                            {/* {measurement} */}
                          </li>
                        </ul>
                      </ul>
                    </li>

                    <li className="treeview cust_inner_li">
                      <a href="javascript:void(0)">

                        <span className="sidetopic">Company Stage</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>

                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '8%' }}>
                          <li>
                            {console.log("new44", this.state.companyAll)}
                            {this.state.companyAll.map(service4 => (
                              (!all_company_statge.includes(service4)) ? (
                                <li onClick={() => this.companyStageClick(service4)} style={{ cursor: 'pointer' }}>{service4}</li>
                              ) : (
                                  <li onClick={() => this.companyStageClick(service4)}>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {service4}</li>
                                )
                            )
                            )
                            }
                            {/* {reng} */}
                          </li>
                        </ul>
                      </ul>
                    </li>
                    <li className="treeview cust_inner_li">
                      <a href="javascript:void(0)">
                        <span className="sidetopic">Certification need</span>
                        <span className="pull-right-container">
                          <i className="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul className="treeview-menu">
                        <ul style={{ margin: '0%', paddingLeft: '8%' }}>
                          <li>

                            {this.state.allCertification.map(service5 => (
                              (!all_certification.includes(service5)) ? (
                                <li onClick={() => this.certificationClick(service5)} style={{ cursor: 'pointer' }}>{service5}</li>
                              ) : (
                                  <li onClick={() => this.certificationClick(service5)}>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    {service5}</li>
                                )
                            )
                            )
                            }
                            {/* {cer} */}
                          </li>
                        </ul>
                      </ul>
                    </li>
                  </ul>

                  <div style={{ marginTop: '15%' }}>
                    <Button type="button" bsStyle="" style={{ width: '38%', backgroundColor: '#4dac4c', color: 'white' }} onClick={this.SubmitApply} >Apply</Button>
                    <Button type="button" bsStyle="" style={{ marginLeft: '19%', width: '38%', backgroundColor: '#1d4e56', color: 'white' }} onClick={this.Resetsubmit}>Reset</Button>
                  </div>


                </div>
              </section>
            </Col>

            <Col md={9}>
              <h2 className="text-center">Select Up to 5 Firms to contact </h2>
              <p className="sidebartext text-center" >contact firms with one click </p>
              <hr className="contact" />
              <div className="row">
                {sidebarpagedata}

              </div>
              <div className="text-center" >
              </div>
            </Col>
          </Col>
          <div className="text-center" >
            <Button className="sidebar-contact" onClick={this.submit} bsStyle="" style={{ marginLeft: '26%', marginTop: '5%', width: '14%', borderRadius: '-15px', fontSize: '24px', color: 'white' }}>Contact Firms</Button>
          </div>

        </div>
      </div>

    )
  }
}
export default firm_list;
