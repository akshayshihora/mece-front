import React, { Component } from 'react';
class Dropdown extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      result: ''
    };
  }

  handleSelectChange = (event) => {
    this.setState({
      result: event.target.value
    })
  }

  render() {
    return (
      <div>
        <h2>Static Dropdown</h2>
        <select onClick={this.handleSelectChange}>
          <option value="select">Select</option>
          <option value="Clang">Clang</option>
          <option value="C++">C++</option>
          <option value="C#">C#</option>
          <option value="Php">Php</option>
          <option value="java">Java</option>
        </select>
        {this.state.result}
      </div>
    );
  }
}
export default Dropdown;
