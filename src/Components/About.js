import React, { Component } from 'react';


class About extends Component {
  render() {


    return (

      <div class="container">
        <div class="page-header">
          <center><h1 id="timeline" style={{ fontWeight: '600' }}>Story about agency</h1></center>
        </div>
        <ul class="timeline">
          <li>
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading text-right">
                <h4 class="timeline-title">Explore options</h4>
              </div>
              <div class="timeline-body">
                <img src="./images/Explore.jpg" width="100%" style={{ paddingTop: '7px' }} />
                <div class="content-one">
                  <p style={{ textAlign: 'center' }}>Search for firms that provide a sepcific service <br />based on various criteria that fits your need.</p>
                </div>
              </div>
            </div>
          </li>

          <li class="timeline-inverted">
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4 class="timeline-title">Select firm to contact</h4>
              </div>
              <div class="timeline-body">
                <img src="./images/select-firm.jpg" width="100%" style={{ paddingTop: '7px' }} />
                <div class="content-two">
                  <p style={{ textAlign: 'center' }}>Using proprietary information, we provide a sorted <br />list of firms that best fit your business need. select upto <br />FIVE firms to contact.</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading text-right">
                <h4 class="timeline-title">Review responses & interview</h4>
              </div>
              <div class="timeline-body">
                <img src="./images/intrview.jpg" width="100%" style={{ paddingTop: '7px' }} />
                <div class="content-three">
                  <p style={{ textAlign: 'center' }}>Review firm solutions to your business need. Interview the <br />firms that have compellingsolution to your problem within <br />your timeline & budget.</p>
                </div>
              </div>
            </div>
          </li>

          <li class="timeline-inverted">
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4 class="timeline-title">Interviews and select firm</h4>
              </div>
              <div class="timeline-body">
                <img src="./images/interview-and-select-firm.jpg" width="100%" style={{ paddingTop: '7px' }} />
                <div class="content-four">
                  <p style={{ textAlign: 'center' }}>Interview the firms that fit your business need and <br />gather answers to all your open questions/ concerns. <br />Select the vendor.</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading text-right">
                <h4 class="timeline-title">Rate firm performance</h4>
              </div>
              <div class="timeline-body">
                <img src="./images/feedback.jpg" width="100%" style={{ paddingTop: '7px' }} />
                <div class="content-five">
                  <p style={{ textAlign: 'center' }}>After the project is completed, please rate the firm on each <br />pillar and provide any additional comments/feedback.</p>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

export default About;
