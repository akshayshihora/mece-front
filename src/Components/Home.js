import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import { Col, Form, FormGroup, FromGroup, FormControl, Button, Image, select, option } from 'react-bootstrap';
import { MultiSelect } from 'react-selectize';
import $ from 'jquery';
import Select from "react-select";

import 'icheck/skins/all.css';
import { Checkbox } from 'react-icheck';


const total_sub_services = [];
const location_array = [];
const customStyles = '';
class Home extends Component {
  customStyles = {
    valueContainer: (provided, state) => ({
      ...provided,
      textOverflow: "unset",
      maxWidth: "90%",
      whiteSpace: "nowrap",
      overflow: "hidden",
      display: "initial"
    })
  };
  multiValueContainer = ({ selectProps, data }) => {
    const label = data.label;
    const allSelected = selectProps.value;
    const index = allSelected.findIndex(selected => selected.label === label);
    const isLastSelected = index === allSelected.length - 1;
    const labelSuffix = isLastSelected ? ` (${allSelected.length})` : ", ";
    const val = `${label} , `;
    return val;
  };
  services = ["Marketing", "Sales", "Risk", "Accounting", "Analytics", "Inovation", "Corporate Development", "Supply Chain", "Finance", "Strategy", "Program Management", "Procurement", "Tax", "Sustainability", "Real Estate", "Technology", "Human Resources"];
  subservices = ["Pricing & Profitabilty Management", "Pricing Strategy ", "Customer & Marketing Strategy", "Customer Insights & Behavourial analytics", "Customer Journey Mapping", "Customer Experience Strategy and Vision", "Markeing Exceution", "Customer Segmenatation", "Brand Management", "Markeing Investment Optimization", "Trade Promotion", "Customer experience capability development"];

  constructor(props) {
    super(props);
    this.state = {
      homedata: {},
      bloglistData: {},
      serviceArea: [],
      service_areas: [],
      services: [],
      location: [],
      subservices: [],
      bttondisabled: true
    }
    this.Submit = this.Submit.bind(this);
    this.test = this.test.bind(this);

  }
  renderValue(option) {
    return <strong style={{ color: option.color }}>{option.label}</strong>;
  }
  getHomeData() {
    $.ajax({
      url: '/homedata.json',
      dataType: 'json',
      cache: false,
      success: function (data) {
        this.setState({ homedata: data });
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(err);
        alert(err);
      }
    });
  }

  serviceArea() {

    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
          this.setState({ serviceArea: data['service_areas']});

      }.bind(this),
      error: function (xhr, status, err) {

      }
  });
   
  }
  Submit() {

    if (!this.state.bttondisabled) {
      $('.spanner').show();
      $.ajax({
        type: 'POST',
        mode: 'no-cors',
        headers: {
          'Content-Type': "application/json",
          'Access-Control-Allow-Origin': '*',
        },
        data: JSON.stringify({ "location": { "latitude": this.state.location[0], "longitude": this.state.location[1] }, "service_areas": this.state.service_areas, "services": this.state.services }),
        url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/mece/api/v1/search',
        success: function (response) {
          console.log(response);
          console.log(response.companies);
          var new_array = [];
          new_array['response'] = response;
          new_array['services'] = this.state.service_areas;
          new_array['subservices'] = this.state.services;
          new_array['allsubservices'] = this.state.subservices;

          //  localStorage.setItem("response",new_array);
          window.sessionStorage.setItem('response', JSON.stringify(response));

          this.props.history.push({

            pathname: '/firm_list',
            data: new_array // your data array of objects
          })
          $('.spanner').hide();
          //this.props.onUpdate(response.companies);
        }.bind(this),
        error: function (xhr, status, err) {
          console.log(xhr.status);
          if (xhr.status == 503) {
            alert("Please try again. No match found.");
            this.props.history.push({
              pathname: '/firm_list',
            })
          }
          $('.spanner').hide();
        }.bind(this),
      });
    }
    else {
      this.setState({ bttondisabled: true });
    }

  }
  componentDidMount() {
    this.initMap();
    this.serviceArea();
    this.setState({ 'isLoading': false });
    //    const total_sub_services = [];
  }


  optionClicked(optionsList) {
    // alert(optionsList);
    console.log(optionsList);
    const new_array = [];
    optionsList.map((k1) => {
      new_array.push(k1.label)
    });
    this.setState({ service_areas: new_array });
    const new_sub_services = [];
    this.state.serviceArea.map(function (myservice1) {
      if (new_array.includes(myservice1.service_area)) {
        myservice1.services.map(function (item) {
          if (!new_sub_services.includes(item.service)) {
            new_sub_services.push(item.service);
          }
        })
      }
    })
    console.log(new_sub_services);
    this.setState({ subservices: new_sub_services });

    if (new_array.length > 0 && this.state.services.length > 0 && typeof this.state.location[0] != "undefined") {
      this.setState({ bttondisabled: false });
    }
    else {
      this.setState({ bttondisabled: true });
    }
  }
  subserviceClicked(optionsList) {
    console.log(optionsList);
    const new_array = [];
    optionsList.map((k1) => {
      new_array.push(k1.label)
    });
    this.setState({ services: new_array });

    if (this.state.service_areas.length > 0 && new_array.length > 0 && typeof this.state.location[0] != "undefined") {
      this.setState({ bttondisabled: false });
    }
    else {
      this.setState({ bttondisabled: true });
    }
  }
  test(place) {
    console.log(place.geometry.location.lng());
    if (typeof place.geometry != 'undefined') {
      location_array.push(place.geometry.location.lat());
      location_array.push(place.geometry.location.lng());
      this.setState({ location: location_array });
      if (this.state.service_areas.length > 0 && this.state.services.length > 0 && location_array.length > 0) {
        this.setState({ bttondisabled: false });
      }
      else {
        this.setState({ bttondisabled: true });
      }
    }
  }

  initMap() {



    var map = new window.google.maps.Map(document.getElementById('automap'), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13
    });

    var input = document.getElementById('pac-input');

    var autocomplete = new window.google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(
      ['address_components', 'geometry', 'icon', 'name']);

    var infowindow = new window.google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new window.google.maps.Marker({
      map: map,
      anchorPoint: new window.google.maps.Point(0, -29)
    });
    autocomplete.addListener('place_changed', (event) => {
      this.test(autocomplete.getPlace());
    });
    autocomplete.addListener('place_changed', function () {
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for location: '" + place.name + "'");
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindowContent.children['place-icon'].src = place.icon;
      infowindowContent.children['place-name'].textContent = place.name;
      infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    });
  }

  componentWillMount() {
    this.getHomeData();
  }


  render() {

    const {
      CustomOption,
      multiValueContainer,
      customStyles,
      ValueContainer
    } = this;
    if (this.state.homedata.blog) {
      var description = this.state.homedata.blog.description;
      var bloglist = this.state.homedata.blog.bloglist.map(function (bloglist) {
        var checkBlogImage = bloglist.image == "" ? "photo2.png" : bloglist.image;
        var bloglistImage = '/images/blog-list-image/' + checkBlogImage;
        return <div class="col-md-12 blog-div-first new_design">
          <Card>
            {/* <CardImg top width="100%" src={bloglistImage} alt="Card image cap" /> */}
            <div className="card-img-top" style={{ backgroundImage: "url(" + bloglistImage + ")" }}></div>
            <div class="blog-post-card-body">
              <CardBody>
                {/* <CardSubtitle><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;{bloglist.date} by <b>{bloglist.author}</b> </CardSubtitle> */}
                <h3 style={{ fontWeight: '600' }}>{bloglist.title}</h3>
                <p className="bloglist-desciption" dangerouslySetInnerHTML={{ __html: bloglist.description }}></p>
                <Link to={{ pathname: '/blog', state: { 'bloglistData': bloglist } }} className="text sidebar-list-more" >More Details</Link>
              </CardBody>
            </div>
          </Card>
        </div>
      })

    }

    if (this.state.homedata.about) {
      var titletag = this.state.homedata.about.titletag;
      var valueproposition = this.state.homedata.about.valueproposition;
      var valueproposition = this.state.homedata.about.valueproposition.map(function (valueproposition) {
        var value = valueproposition.value;
        var checkImage = valueproposition.image == "" ? "photo2.png" : valueproposition.image;
        var aboutlistImage = '/images/' + checkImage;

        if (value == 1) {
          return <div class="col-md-4 ">
            <Card className="valuepropostion">
              <CardImg top width="35%" src={aboutlistImage} alt="Card image cap" />
              <CardBody>
                <CardText className="CardText1">{valueproposition.description}</CardText>
              </CardBody>
            </Card>
          </div>

        }
        else {
          return <div class="col-md-4 ">
            <Card className="valuepropostion1">
              <CardImg top width="35%" src={aboutlistImage} alt="Card image cap" />
              <CardBody>
                <CardText className="CardText1">{valueproposition.description}</CardText>
              </CardBody>
            </Card>
          </div>
        }

      })

    }


    if (this.state.homedata.keybenifits) {
      var keybenifits = this.state.homedata.keybenifits.map(function (keybenifits) {
        var keybeifitsImage = '/images/' + keybenifits.image;
        return <div class="col-md-2 col-md-offset-1 keybeni keybeni-first">
          <Image style={{ width: '45%' }} src={keybeifitsImage} />
          <h4 style={{ fontWeight: 'bold', color: '#1b3f69', fontSize: '25px' }}>{keybenifits.title}</h4><br />
          <p style={{ color: '#1b3f69', fontSize: '18px' }} className="resp-keybenifits">{keybenifits.description}</p>
        </div>
      })
    }

    if (this.state.homedata.servicearea) {
      var servicearea = this.state.homedata.servicearea;
      var services = this.state.homedata.services;
    }
    if (this.state.serviceArea.length > 0) {

    }
    if (this.state.homedata.team) {
      var team = this.state.homedata.team.description;
      var titlefooter = this.state.homedata.team.titlefooter;
    }

    return (
      <div class="container-fluid">

        <div className="banner-form-div ">
          <div className="row">
            <div className="col-md-10 col-md-offset-1">
              <h2 className="header-quote-h2" style={{ textAlign: 'center' }}>Explore and hire management consulting firms</h2>
            </div>
            <div className="col-md-12 home_ms_col12" style={{ marginTop: '2%' }}>
              <Col md={4} sm={4} xs={12} className="home_ms_col4">

                {<Select

                  isMulti
                  components={{
                    MultiValueContainer: multiValueContainer,
                    //  Option: CustomOption,
                  }}
                  closeMenuOnSelect={false}
                  hideSelectedOptions={false}
                  valueRenderer={this.renderValue}
                  styles={customStyles}
                  isSearchable={false}
                  className="select-one"
                  placeholder="Services"
                  name="service"
                  trim={true}

                  onChange={this.optionClicked.bind(this)}
                  options={this.state.serviceArea.map(
                    service => ({ label: service.service_area, value: service.service_area })

                  )}
                />}
              </Col>


              {<Col md={4} sm={4} xs={12} className="home_ms_col4">


                <Select

                  isMulti
                  components={{
                    MultiValueContainer: multiValueContainer
                    // Option: CustomOption,
                  }}
                  closeMenuOnSelect={false}
                  hideSelectedOptions={false}
                  styles={customStyles}
                  isSearchable={false}
                  className="select-one"
                  placeholder="Sub-services"
                  name="subservice"
                  onChange={this.subserviceClicked.bind(this)}

                  options={this.state.subservices.map(
                    service => ({ label: service, value: service })
                  )}
                />


              </Col>}


              <Col md={2} sm={2} xs={12} className="home_ms_col4">
                <FormGroup style={{ marginTop: '5px' }}>
                  <FormControl className="" id="pac-input" type="text" placeholder="Location" style={{ minWidth: '0px' }} />
                </FormGroup>
              </Col>
              <Col md={2} sm={2} xs={12} className="home_ms_col4">
                <FormGroup style={{ marginTop: '5px' }}>

                  <Button className="btn-explore btn-lg" style={{ marginLeft: '0px' }} disabled={this.state.bttondisabled ? 'disabled' : ''} onClick={this.Submit}>Explore</Button>

                </FormGroup>
              </Col>
            </div>
          </div>
        </div>
        <div class="col-md-offset-2 col-md-8 col-md-offset-2" style={{ zIndex: '-1' }}>
          <div className="row step-div">
            <h1>MECE Inc. Works in FIVE Steps</h1>
            <Image src="./images/Steps1.png" className="stepimg" />
          </div>
        </div>

        <div class="col-md-12" style={{ marginTop: '-8%' }}>
          <div className="row keyfeature-div">
            <br />
            <h1>Key Benefits</h1>
            <hr style={{ width: '6%', marginLeft: '47%', borderColor: 'black' }} />
            <br />
            <div class="col-md-12 text-center">
              <div class="col-md-2 col-md-offset-1 keybeni keybeni-first" >
                <Image style={{ width: '45%' }} src="./images/Group1.png" />
                <h4 style={{ fontWeight: 'bold', color: '#1b3f69', fontSize: '25px' }}>You are in charge. </h4><br />
                <p style={{ color: '#1b3f69', fontSize: '18px', }} className="resp-keybenifits">Search for vendors that fit your need.Set the objectives and desired results.
                </p>
              </div>
              <div class="col-md-2 col-md-offset-1 keybeni" style={{ marginLeft: '5%' }}>
                <Image style={{ width: '45%' }} src="./images/icn/2.png" />
                <h4 style={{ fontWeight: 'bold', color: '#1b3f69', fontSize: '25px' }}>You connect in one portal. </h4><br />
                <p style={{ color: '#1b3f69', fontSize: '18px', }} className="resp-keybenifits">Select potential vendors to receive responses from with
                a click of a button.</p>
              </div>
              <div class="col-md-2 col-md-offset-1 keybeni" style={{ marginLeft: '5%' }}>
                <Image style={{ width: '45%' }} src="./images/icn/3.png" />
                <h4 style={{ fontWeight: 'bold', color: '#1b3f69', fontSize: '25px' }}>You get to compare.</h4><br />
                <p style={{ color: '#1b3f69', fontSize: '18px' }} className="resp-keybenifits">Review the vendor responses
                 and rate them.</p>
              </div>
              <div class="col-md-2 col-md-offset-1 keybeni" style={{ marginLeft: '5%' }}>
                <Image style={{ width: '45%' }} src="./images/icn/4.png" />
                <h4 style={{ fontWeight: 'bold', color: '#1b3f69', fontSize: '25px' }}>You get to choose.</h4><br />
                <p style={{ color: '#1b3f69', fontSize: '18px' }} className="resp-keybenifits">Interview and select vendor. </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" style={{ height: '75px', backgroundColor: '#edf2f5' }} id="valuepropostion">

          </div>
          <div class="col-md-12" id="" style={{ backgroundColor: '#030542', padding: '30px' }}>
            <div class="col-md-10">
              <p style={{ float: 'left' }} className="value-top-p">{titletag}</p>
            </div>
            <div class="col-md-2" style={{ textAlign: 'center' }}>
              <Link to="/Explore"><Button className="col-md-12 btn btn-success btn-lg">Explore</Button></Link>
            </div>
          </div>
        </div>

        <div class=" col-md-offset-1 col-md-10 col-md-offset-1 value-proposition-div">
          <div class="row">

            <div class="col-md-12">
              <h1>Value Proposition </h1>
              <hr style={{ width: '6%', marginLeft: '47%', borderColor: 'black' }} />
              {valueproposition}
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <div className="col-md-12 team">

            <h1 className="team-h1">Team</h1>
            <hr style={{ width: '6%', marginLeft: '47%', borderColor: 'black' }} />
            <p className="team-p" style={{ textAlign: 'justify', margin: '0% 5%' }}><span class="left-team">"</span>{team}"</p>
            <div className="footer-team">
              {titlefooter}
            </div>
            <div className="col-md-12">
            </div>
          </div>
        </div>

        <div id="storyabout" class="col-md-12 story-about-agency-div" style={{ backgroundImage: 'url(./images/about.jpg)', backgroundSize: 'cover' }}>
          <h1> Connect & contract with management consulting Firm. </h1>
          <hr style={{ width: '6%', marginLeft: '46%' }} />
          <div class="col-md-12" style={{ marginTop: '3%' }}>

            <div class="col-md-4 text-center story-1 ">
              <Image src="./images/1.png" />
              <h4 style={{ fontWeight: 'bold' }}>1.&nbsp;Explore options</h4>
              <p className="stry-1">Search for firms that provide a
               specific service based on various
               criteria that fits your need.</p>
            </div>
            <div class="col-md-4 text-center story-1" >
              <Image src="./images/2.png" />
              <h4 style={{ fontWeight: 'bold' }}>2.&nbsp;Select firms to contact</h4>
              <p className="stry-2">Using proprietary information, we provide a pre-sorted list of firms that best fit your business need. Select the firms to contact with a single click.</p>
            </div>
            <div class="col-md-4 text-center story-1">
              <Image src="./images/3.png" />
              <h4 style={{ fontWeight: 'bold' }}>3.&nbsp;Review responses and interview</h4>
              <p className="stry-3">Review firms’ solutions to your business need. Interview the firm(s) that have compelling solutions to your problem within you timeline & budget. </p>
            </div>
          </div>

          <div class="col-md-12 second-line-agency-div">
            <div class="col-md-offset-2 col-md-4 text-center story-1">
              <Image src="./images/4.png" />
              <h4 style={{ fontWeight: 'bold' }}>4.&nbsp;Interview and select firms</h4>
              <p className="stry-4">Interview the firms that fit your business need and gather answer to all your open questions/concerns. Select the vendor.</p>
            </div>
            <div class="col-md-4 text-center story-1">
              <Image src="./images/5.png" />
              <h4 style={{ fontWeight: 'bold' }}>5.&nbsp;Rate firm performance</h4>
              <p className="stry-5">After the project is completed, please
               rate the firm on each pillar and provide
                 any additional comments/feedback.</p>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>


        <div class=" col-md-offset-1 col-md-10 col-md-offset-1 blog-post-div" style={{ marginTop: '-4%' }}>
          <h1> Recommended Blog </h1>
          <hr style={{ width: '10%', marginLeft: '45%', borderColor: 'black' }} />
          <h4 class="col-md-offset-2 col-md-8">{description}</h4>
          <br />
          {bloglist}
        </div>


        <div class="col-md-12 map-image-div" id="contact-map-form" style={{ marginBottom: '-6%' }} >
          <div class="map-image">
            <div class="row">
              <form class=" map-form pull-right">
                <div class="col-md-12">
                  <center><h3 className="abc"> Contact Us </h3></center>

                  <div class="col-md-6">
                    <h5 style={{ fontWeight: 'bold' }}>
                      <Image src='./images/call1.png' style={{ width: '30px' }} />
                      &nbsp; Give us a ring</h5>
                    <p style={{ marginLeft: '18%', color: '#a79797' }}>
                      (304) 933 – 9384<br />
                      Mon-Fri,8:00-22:00
                  </p>

                  </div>
                  <div class="col-md-6">
                    <h5 style={{ fontWeight: 'bold' }}>
                      <Image src='./images/loc.png' style={{ width: '30px' }} />&nbsp;
                    Find us at the office</h5>
                    <p style={{ marginLeft: '18%', color: '#a79797' }}>10601 Pecan Park Blvd #202,
                    Austin, TX 78750
                   </p>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="col-md-6">
                    <input style={{ minWidth: '0px', padding: '0px' }} type="text" name="fullname" placeholder="Full Name" className="contact-us-text" />
                    <input style={{ minWidth: '0px', padding: '0px' }} type="text" name="email" placeholder="Email" className="contact-us-text" />
                  </div>
                  <div className="col-md-6">
                    <input style={{ minWidth: '0px', padding: '0px' }} type="text" name="companyname" placeholder="Company Name" className="contact-us-text" />
                    <input style={{ minWidth: '0px', padding: '0px' }} type="text" name="phone" placeholder="Phone" className="contact-us-text" />
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="col-md-12">
                    <textarea style={{ minHeight: '0px', width: '100%' }} placeholder="Your Message" className="contact-us-text"></textarea><br />
                  </div>
                </div>


                <div class="col-md-12">
                  <div class="col-md-6 desktop-icheck">
                    <Checkbox
                      id="checkbox1"
                      checkboxClass="icheckbox_square-green contact-checkbox"
                      increaseArea="20%"
                    />&nbsp;&nbsp;<span style={{ color: 'grey' }}>I'm not a robot</span>

                  </div>
                  <div class="col-md-6">
                    <button class="btn btn-warning snd-msg" style={{ marginLeft: '36%', background: '#4dac4c', borderColor: '#4dac4c', boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 -5px 10px 0 rgba(0, 0, 0, 0.12)' }}>Send Message</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
        <div id="automap" style={{ height: '500px', position: 'relative', top: '20%', width: '100%', display: 'none' }}></div>
        <div id="infowindow-content">
          <img src="" width="16" height="16" alt="" id="place-icon" />
          <span id="place-name" class="title"></span><br />
          <span id="place-address"></span>
        </div>
      </div>

    );
  }
}

export default Home;
