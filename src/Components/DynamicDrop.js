import React, { Component } from 'react';
class DynamicDrop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: [
        { name: 'purity', id: 1 , surname: 'parmar'},
        { name: 'black', id: 2 , surname: 'black'},
        { name: 'goldi', id: 3 , surname: 'goldi'},
        { name: 'yellow', id: 4 , surname: 'parmar'},
        { name: 'red', id: 5 , surname: 'parmar' },
        { name: 'purple', id: 6 , surname: 'purple' }
      ]
    };
  }
   

  render() {
    let optionTemplate = this.state.values.map(v => (
      <option value={v.id}>{v.name}{v.surname}</option>
    ));

    return (
      <label>
        Pick your favorite Number:
        <select value={this.state.value} onChange={this.handleChange}>
          {optionTemplate}
        </select>
      </label>
    );
  }
}

export default DynamicDrop;