import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Col, Row, Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
class Demo extends Component {
  render() {


    return (

      <div class="container">
        <div class="page-header">
          <center><h1 id="timeline" style={{ fontWeight: '600' }}>How MECE works for you</h1></center>
        </div>
        <ul class="timeline">
          <li>
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading text-right">
                <h4 class="timeline-title">Explore options</h4>
              </div>
              <div class="timeline-body">
                <div className="cust_demo_img" style={{ backgroundImage: "url(./images/Explore.jpg)" }}></div>
                {/* <img src="./images/Explore.jpg" width="100%" style={{paddingTop: '7px'}}/> */}
                <div class="content-one">
                  <p style={{ textAlign: 'center' }}>Search for firms that provide a specific service <br />based on various criteria that fits your need.</p>
                </div>
              </div>
            </div>
          </li>

          <li class="timeline-inverted">
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4 class="timeline-title">Select firms to contact</h4>
              </div>
              <div class="timeline-body">
                <div className="cust_demo_img" style={{ backgroundImage: "url(./images/select-firm.jpg)" }}></div>
                {/* <img src="./images/select-firm.jpg" width="100%" style={{paddingTop: '7px'}}/> */}
                <div class="content-two">
                  <p style={{ textAlign: 'center' }}>Using proprietary information, we provide a sorted <br />list of firms that best fit your business need. Select the <br />firms to contact with a single click.</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading text-right">
                <h4 class="timeline-title">Review responses & interview</h4>
              </div>
              <div class="timeline-body">
                <div className="cust_demo_img" style={{ backgroundImage: "url(./images/intrview.jpg)" }}></div>
                {/* <img src="./images/intrview.jpg" width="100%" style={{paddingTop: '7px'}}/> */}
                <div class="content-three">
                  <p style={{ textAlign: 'center' }}>Review firms’ solutions to your business need.<br /> Interview the firm(s) that have compelling solutions<br /> to  your problem within you timeline & budget.</p>
                </div>
              </div>
            </div>
          </li>

          <li class="timeline-inverted">
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading">
                <h4 class="timeline-title">Interviews and select firm</h4>
              </div>
              <div class="timeline-body">
                <div className="cust_demo_img" style={{ backgroundImage: "url(./images/interview-and-select-firm.jpg)" }}></div>
                {/* <img src="./images/interview-and-select-firm.jpg" width="100%" style={{paddingTop: '7px'}}/> */}
                <div class="content-four">
                  <p style={{ textAlign: 'center' }}>Interview the firms that fit your business need and gather <br /> answer to all your open questions/concerns. Select the vendor.</p>
                </div>
              </div>
            </div>
          </li>

          <li>
            <div class="timeline-badge"></div>
            <div class="timeline-panel">
              <div class="timeline-heading text-right">
                <h4 class="timeline-title">Rate firm performance</h4>
              </div>
              <div class="timeline-body">
                <div className="cust_demo_img" style={{ backgroundImage: "url(./images/feedback.jpg)" }}></div>
                {/* <img src="./images/feedback.jpg" width="100%" style={{paddingTop: '7px'}}/> */}
                <div class="content-five">
                  <p style={{ textAlign: 'center' }}>After the project is completed, please rate the firm on each <br />pillar and provide any additional comments/feedback.</p>
                </div>
              </div>
            </div>
          </li>
        </ul>

        <div className="text-center">
          <Link to="/Explore"><Button type="button" className="btn-contact" bsStyle="" bsSize="large" style={{ backgroundColor: '#4dac4c', color: 'white' }}> Try it out and see all your options</Button></Link>
        </div>
      </div>
    );
  }
}

export default Demo;
