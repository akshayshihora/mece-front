
import React  from 'react';
class Terms extends React.Component {
   constructor() {
        super();
 	
    }
render() 
{
    return (
    	 <div class="container" style={{paddingTop: '15px'}}>
      		<p><b>MECE Inc. PRIVACY POLICY</b> <br/>Effective:&nbsp;February 19, 2019 <br/> (Subject to Revision) </p>
			<p>This Privacy and Security Policy (this &ldquo;Privacy Policy&rdquo;) explains how we collect, protect, use, and disclose the Private Information (defined below) when you use our website located at&nbsp;www.MECEInc.com or any other website we operate (collectively the &ldquo;Sites&rdquo;), including without limitation those websites through which&nbsp;our customers,&nbsp;members,&nbsp;subscribers, users, providers, associates,&nbsp;advertisers,&nbsp;contractors and business partners may complete transactions, conduct other business and manage their accounts. We understand that you care about how your Private Information is used and shared, and we take your privacy seriously. Please read the following to learn more about this Privacy Policy.&nbsp;</p>

			<p><strong>GENERAL</strong></p>

			<p>&ldquo;MECE&rdquo;, &ldquo;we&rdquo; and&nbsp;&ldquo;our&rdquo; refer to&nbsp;MECE&nbsp;INC.&nbsp;and its operating subsidiaries and affiliates, if any, that adopt this Privacy Policy. &ldquo;You&rdquo; and &ldquo;your&rdquo; refer to the individuals visiting our sites, as well as to the organizations or businesses that they represent.&nbsp;</p>

			<p>By visiting one of the Sites, you are accepting the practices outlined in this Privacy Policy, which is subject to modification as described below.&nbsp;If you do not agree to the terms and conditions of this Privacy Policy, including having your personally identifiable information (&quot;Private Information&quot; as defined below) used in any of the ways described in this Privacy Policy, you may not be able to use certain parts or features of our Sites and services, and in some instances, this may necessitate the revocation of your membership. It is your responsibility to review this Privacy Policy frequently and remain informed about any changes to it, so we encourage you to visit this page often.</p>

			<p>WHAT IS PRIVATE INFORMATION?</p>

			<p><strong>&ldquo;Private Information&rdquo;</strong> means</p>

<p>&bull; as to a natural person, any information that may be used uniquely to identify the person (other than in such person&rsquo;s representative capacity on behalf of a business enterprise), including such information submitted in&nbsp;candidate&nbsp;profiles in connection with employment applications through our employment or recruitment pages. Such information includes first and last name, birth dates, social security number, driver&rsquo;s license, gender, race, personal (as opposed to business) e-mail addresses, credit information, home phone numbers or addresses, employment and educational history.</p>

<p>&bull; as to a business enterprise, any banking or confidential financial information that it does not generally share with third parties but that is shared with us in any credit application process or business transaction.&nbsp;</p>

<p>&ldquo;Private information&rdquo; does not include, without limitation, aggregated information (i.e. information presented in summary or statistical form which does not contain data that would permit the identification of a specific individual without significant effort).</p>

<p>A &ldquo;business enterprise&rdquo; or &ldquo;business&rdquo; means any sole proprietorship, partnership, corporation, limited partnership, limited liability partnership or company, business group or cooperative.</p>

<p><strong>HOW&nbsp;MECE GATHERS PRIVATE INFORMATION</strong></p>

<p>MECE&nbsp;may collect Private Information from its customers, providers and other Site users in several ways:</p>

<p><strong>MECE may gather account information through the Sites.</strong></p>

<p>When you visit the Sites, we may ask you for Private Information so that we can provide you with the products, services, or information that you have requested, including credit and financial information required in order to open a business account with us. When you maintain a business account with&nbsp;MECE&nbsp;and transact business with&nbsp;MECE&nbsp;through the Sites, we will ask you for information necessary to process and complete transactions with us, including your name, shipping and billing addresses, e-mail address, and credit card or other payment-related information.&nbsp;</p>

<p><strong>MECE&nbsp;may maintain historical account information of its customers and&nbsp;providers.</strong></p>

<p>MECE may maintain historical account information for its present and former customers and&nbsp;providers. Often this historical information predates such customers&rsquo; and&nbsp;providers&rsquo; use of the Sites for transacting business with&nbsp;MECE. We maintain this information so that we may better serve and effectively interact with our customers and&nbsp;providers. Historical account information constituting Private Information maintained by&nbsp;MECE is subject to this Privacy Policy.</p>

<p><strong>MECE&nbsp;may gather information about individual guarantors.</strong></p>

<p>In connection with credit terms extended to our customers, we may collect and maintain Private Information about individuals who give personal guarantees on behalf of businesses, such as when the owner of a small&nbsp;business&nbsp;provides a personal guarantee on behalf of&nbsp;such business.&nbsp;</p>

<p><strong>MECE&nbsp;may gather information from the Sites by cookies and Internet monitoring technologies.</strong></p>

<p>Cookies are small text files stored by your browser in your computer when you visit our Sites. We use cookies to improve the Sites and make them easier to use. Cookies permit us to recognize users and avoid repetitive requests for the same information. Cookies also assist us in identifying the types of browsers and operating systems used most by our customers, and how visitors move through the Sites. All of this information enables us to improve the Sites and tailor them to our customers&rsquo; needs and preferences. We use cookies for a variety of purposes intended to make our Sites a better service for you. We reserve the right to use cookies to deliver content specific to your needs and interests. We may also use cookies to track user trends and patterns in order to better understand and improve areas of our sites that our users find valuable. In addition, our servers track various technical details about your visit, such as your computer&rsquo;s Internet protocol address and the pages that you visited.&nbsp;Most browsers will accept cookies until you change your browser settings to refuse them. At any time, you can disable the use of cookies in your browser and still have access to much of the information on the Sites. However, you may not be able to access your account information or complete transactions on the Site unless your permit the use of cookies.&nbsp;We also reserve the right to use outside companies to display ads on our sites. These ads may contain cookies. Cookies received with banner ads are collected by such outside companies, and we do not have access to this information.</p>

<p><strong>MECE&nbsp;may gather information provided by you in one or more other features of the Sites.</strong></p>

<p>MECE may from time to time offer interactive or social aspects on the Sites. For example, if you choose to express an opinion in an online forum, respond to user polls, or enter any contests, we may ask you for Private Information. If you request to sign up for newsletters or other promotional materials, or if you send an inquiry to one of our customer service representatives, we may store a record of your request along with any Private Information allowing us to respond to your inquiry.&nbsp;</p>

<p><strong>HOW&nbsp;MECE USES PRIVATE INFORMATION</strong></p>

<p><strong>MECE&nbsp;uses Private Information in establishing, maintaining and concluding business or employment arrangements.</strong></p>

<p>MECE uses Private Information to evaluate prospective customers,&nbsp;providers and employees and, if we so elect, to open, maintain and terminate customer accounts, supplier arrangements, or employment relationships.</p>

<p><strong>MECE&nbsp;uses Private Information to process&nbsp;transactions with customers, subscribers, users and members and to improve its business processes.</strong></p>

<p>MECE uses Private Information to process payments&nbsp;made&nbsp;by customers,&nbsp;subscribers,&nbsp;users, and members and&nbsp;to respond to the requests of users of the Sites, and generally to enrich the usefulness of the Sites. In addition,&nbsp;MECE may use Private Information, both on an identifiable and aggregate basis, in various analyses intended to help&nbsp;MECE understand its customers, members, and providers better.&nbsp;&nbsp;MECE reserves the right to perform such &ldquo;data mining&rdquo; activities at its discretion, subject to the terms of this Privacy Policy.&nbsp;</p>

<p><strong>MECE&nbsp;uses Private Information in aggregated reports.</strong></p>

<p>We may combine Private Information with other information to create aggregate or summary reports, and we may provide aggregate data to other parties for marketing, advertising, and other purposes. If&nbsp;MECE provides information to other parties in aggregate form, such data will not include any specifically identifiable Private Information concerning specific customers,&nbsp;providers or individuals.</p>

<p><strong>IS PRIVATE INFORMATION EVER SHARED OR DISCLOSED?</strong></p>

<p>Private Information is used to complete and support your use of the Sites and the services provided thereon and to comply with any requirements of law.&nbsp;&nbsp;MECE may share Private Information with third parties as described below:</p>

<p><strong>MECE may disclose Private Information as required by law and for the protection of&nbsp;MECE&nbsp;and others.&nbsp;</strong></p>

<p>MECE may disclose Private Information if required to do so by law or in the good faith belief that such action is necessary to: (1) conform to the edicts of the law or comply with legal process served on&nbsp;MECE or its affiliates or the Sites; (2) protect, enforce and defend the rights, property or safety of&nbsp;MECE, our employees and contractors, our users or others; (3) act in urgent circumstances to protect the personal safety of users of&nbsp;MECE, its web sites or the public; and (4) enforce or apply our terms of use, credit terms, and other business agreements and arrangements. Without limiting the foregoing, we may use&nbsp;Private Information in connection with the collection or enforcement of your obligations to us. We may also share Private Information with other companies and organizations for fraud protection and credit risk reduction.&nbsp;</p>

<p><strong>MECE may disclose Private Information to third parties who help us provide our services.&nbsp;</strong></p>

<p>We depend upon third parties and independent contractors (collectively &ldquo;third party service providers&rdquo;) to provide services in connection with our business operations. We may transmit your Private Information to or allow access to such information by third party service providers who assist us in a variety of ways, including in hosting our business information and data, allowing information technology providers and maintenance services to provide and support information technology systems and services, processing orders and payments, shipping products, sending catalogs, performing surveys, conducting promotions, advertising and marketing, processing job applications, providing recruitment and background screening, and sending e-mails and newsletters. Without limiting the foregoing, we may make certain of the pages associated with our sites available for employment opportunities or human resources. They usually will be captioned &quot;Careers&quot; or &quot;Career Opportunities&quot; or will have similar labels or captions clearly indicating the employment opportunity subject matter and such pages may be co-branded with third party service providers. For example, we have contracted with various background search, testing and drug screening companies that may provide recruitment related services at different points in the recruiting process.</p>

<p>We use good faith efforts with these&nbsp;third party service providers to require them to protect the privacy of Private Information by implementing adequate security measures and by using the Private Information only as authorized and directed by&nbsp;MECE. We do not permit such third parties to use Private Information for direct marketing or any other purposes not authorized by&nbsp;MECE. However, such&nbsp;third party service providers perform work outside of our direct supervision and, therefore, we can never guarantee that Private Information will be protected by such third parties.</p>

<p><strong>MECE may disclose Private Information as part of a corporate transaction.&nbsp;</strong></p>

<p>If&nbsp;MECE should ever merge with, acquire or be acquired by another company, whether by merger, stock transfer, or sale of assets, we may disclose, sell or otherwise transfer control of the Private Information to a third party as part of such transaction.</p>

<p><strong>Private Information may be disclosed if you participate in one or more interactive or social features that&nbsp;MECE may offer on the Sites.&nbsp;</strong></p>

<p>MECE&nbsp;may&nbsp;allow other users of the Sites to browse comments, questions or other entries that you have submitted or commented on in one or more forums, message boards, product ratings, feedback portals or other interactive or social aspects that&nbsp;MECE may include on the Sites from time to time.&nbsp;&nbsp;MECE may choose to post this data on other features in the future. Your use of such social features constitutes your consent and agreement to&nbsp;MECE&rsquo;s use of any content you post or transmit through such features for&nbsp;MECE&rsquo;s editorial, advertising and publicity purposes, without compensation to you, except where prohibited by law.</p>

<p><strong>IS AGGREGATED INFORMATION EVER SHARED OR DISCLOSED?</strong></p>

<p>We reserve the right to share aggregated information with third parties at our discretion. We will use our good faith efforts to aggregate information in a manner that will not reveal identifiable Private Information about you.</p>

<p><strong>OPT OUT PROCEDURES</strong></p>

<p>To the extent required by law, and except as set forth above, so long as you have provided your email address to us, if we intend to use or share your Private Information beyond the scope described above or in a manner that we reasonably determine to be a purpose incompatible with the purpose(s) for which it was originally collected, we will endeavor to contact you and provide you with the ability to prevent the sharing of this information through &ldquo;opt out&rdquo; elections. If we do provide opt out elections with respect to new uses, even if not legally required to do so, such opt out elections will be treated as affirmative consent to the specified uses if you do not respond in a manner and within the period specified in the election to deny us the right to make such use.</p>

<p><strong>IS PRIVATE INFORMATION SECURE?</strong></p>

<p>The security of Private Information is a priority for us. We implement measures that we believe in good faith will protect this information to the extent reasonably feasible in light of industry practices. In some cases the measures employed may vary depending on the level of sensitivity of the Private Information. Our measures may include restricted access to certain of our systems (including physical and electronic safeguards, such as passwords, encrypting sensitive information (as described below)). We may also restrict user access commensurate with responsibilities and institute procedural safeguards, such as identity authentication procedures. We train our employees in the proper handling of Private Information. We also maintain a disaster recovery and continuity plan to help guard against unforeseen disruptions. When you open business accounts or enter into business transactions through the Sites,&nbsp;MECE uses Secure Sockets Layer (SSL) technology. SSL encrypts your credit card and other Private Information before it is transmitted to us to limit reading or recording of such information as it travels over the Internet. Credit card information submitted by a user or gained through normal financial transactions is governed by financial guidelines and federal, state and/or local laws.&nbsp;&nbsp;MECE holds all credit card information in strict confidence and for the sole purpose of conducting business with&nbsp;MECE, all in accordance with Payment Card Industry (PCI) guidelines.&nbsp;We may also use encryption technology for certain Private Information used internally within our company, as determined on a case by case basis. Generally, we do not encrypt information deemed &ldquo;public,&rdquo; such as information publicly available on the Sites and not protected by passwords or other security measures.&nbsp;</p>

<p>We may share credit card information with trusted third parties in order to bill you for our service. We use third parties for secure credit card transaction processing, and we send billing information to those third parties to process your orders and payments or to validate your credit card information on file. Credit card information is encrypted during transmission and whenever stored within our systems or with our vendors. In addition to utilizing encryption that meets the US Government&rsquo;s FIPS 140-2 Certification, we take care to store only the amount of information required to bill you for services.</p>

<p>Our credit card processing vendors use security measures to protect your information both during the transaction and after it&rsquo;s complete. Credit card processing vendors are certified as compliant with multiple industry-standard compliance frameworks including the Payment Card Industry (PCI) Service Provider Level 1, the SSAE-16 audit standard.</p>

<p><strong>When we share Private Information with third party service providers, we use good faith efforts to require such service providers to protect the privacy of Private Information by implementing adequate security measures and by using the Private Information only as authorized and directed by us. However, such&nbsp;third party service providers perform work outside of our direct supervision and, therefore, we can never guarantee that Private Information will be protected by such third parties. Use of the Sites is at your own risk. We are not responsible for the circumvention of any security measures used by our&nbsp;third party service providers.&nbsp;</strong></p>

<p>If you have or open a business account with&nbsp;MECE, we have implemented&nbsp;or may implement&nbsp;separate policies, protocols, procedures and agreements with respect to the use of passwords and other authenticating information (the &ldquo;Authentication and Attribution Policies&rdquo;). Among other things, the privacy and security of your&nbsp;MECE account information is protected by your account user name and password. You should be aware that any person to whom you grant access to your account user name password will have access to your complete account information, including information such as credit information, information about individuals including individual guarantors, and transaction histories. You should not grant access to your user name and password to any person to whom you are not comfortable sharing such information. In addition, you need to work to protect against unauthorized access to your password and to your computer by logging off once you have finished using a shared computer.&nbsp;</p>

<p><strong>LINKS TO OTHER SITES</strong></p>

<p>You should be aware that this Privacy Policy applies only to the Sites. Importantly, it does not apply to any other websites to which a link may be provided on the Sites. We cannot control and are not responsible for the actions of third parties operating such sites. You should not take the existence of an affiliation with, or a link from, the Sites to any such other website to mean that it has a privacy policy similar to this one. You should review the privacy policy of any such website.</p>

<p><strong>INTERNATIONAL TRANSFERS</strong></p>

<p>Private Information collected on this web site may be stored and processed in the United States or any other country in which&nbsp;MECE or its affiliates, subsidiaries, agents, or&nbsp;third party service providers maintain facilities and/or infrastructure, and by using this web site, you consent to any such transfer of information outside of your country. Unless applicable law requires different security and protections to be applied in any given relevant jurisdiction, we apply the same principles in using, handling and transmitting data as described in this Privacy Policy in all of our operations.</p>

<p><strong>OTHER AGREEMENTS, ARRANGEMENTS AND POLICIES</strong></p>

<p>This Privacy Policy supplements, but does not supersede, the provisions set forth in the Terms of Use&nbsp;relating to Sites, Authentication and Attribution Policies, supplier or customer agreements, credit applications, and other agreements, arrangements or policies, which provisions shall take precedence over any conflicting terms in this Privacy Policy.</p>

<p><strong>MODIFICATIONS TO THIS PRIVACY POLICY; YOUR CONSENT</strong></p>

<p>By using any of the Sites, you agree to this Privacy Policy. This is our entire and exclusive Privacy Policy and it supersedes any earlier version. We may change this Privacy Policy at any time by posting a new version of this Privacy Policy on the Sites. We encourage you to periodically review this Privacy Policy to stay informed about how we are protecting the Private Information we collect. Your continued use of a Site constitutes your agreement to this Privacy Statement and any updates. We may also obtain your consent to updates through the use of &ldquo;opt out&rdquo; procedures. See &ldquo;Opt Out Procedures&rdquo; above.</p>

<p><strong>DISCLAIMER; APPLICABLE LAW; LIMITATIONS ON LIABILITY</strong></p>

<p>The Sites operate &ldquo;AS-IS&rdquo; and &ldquo;AS-AVAILABLE,&rdquo; without liability of any kind. We are not responsible for events beyond our direct control. This Privacy Policy is governed by the laws of Texas, excluding conflicts of law principles.&nbsp;&nbsp;<strong>Any legal actions against us must be commenced in the state or federal courts located in&nbsp;Williamson County,&nbsp;Texas within one (1) year after the claim arose, or such action(s) will be barred.&nbsp;</strong></p>

<p><strong>CALIFORNIA PRIVACY RIGHTS</strong></p>

<p>California Civil Code Section 1798.83 permits users that are California residents to request that we not share certain information included in &ldquo;Private Information&rdquo; with third parties if we know or have reason to know that the third parties use the covered information for the third parties&rsquo; direct marketing purposes. As stated above under the caption&nbsp;<strong>&ldquo;MECE may disclose Private Information to third parties who help us provide our services&rdquo;</strong>&nbsp;our policies and contracts with third party service providers require such providers to secure Private Information related to all customers and&nbsp;providers, including California customers and&nbsp;providers, and do not permit any use of Private Information for direct marketing or any other purposes not authorized by&nbsp;MECE.</p>

<p><strong>A NOTE ABOUT THE PRIVACY OF CHILDREN UNDER 13&nbsp;</strong></p>

<p>None of our Sites are directed or otherwise promoted for use by children under the age of thirteen (13). Consistent with the Children&rsquo;s Online Privacy Protection Act of 1998,&nbsp;MECE does not knowingly collect personally identifiable information on children under the age of thirteen (13).&nbsp;</p>

<p><strong>CONTACTING US</strong></p>

<p>If you believe that&nbsp;MECE has not adhered to this Privacy Policy, or if you feel that you need to correct information about you held by&nbsp;MECE, please contact us by email&nbsp;at&nbsp;<a href="mailto:info@meceinc.com">info@meceinc.com</a>&nbsp;or by postal mail at&nbsp;MECE&nbsp;INC.,&nbsp;10601 Pecan Park Blvd., Suite 202, Austin, Texas 78750.</p>

    	</div>
    );
}
}
export default Terms;

