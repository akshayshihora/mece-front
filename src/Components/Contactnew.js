import React from 'react';
import './reg.css';
import {Button,FormGroup,FormControl,ControlLabel} from 'react-bootstrap';
import { BrowserRouter as Router, Route,Link } from 'react-router-dom';

class Contactnew extends React.Component {
   constructor() {
        super();
  
    }
render() 
{
    return (

      <div className="container">
        <div className="row">
          <div className="col-md-12 col-sm-12" >

              <div className="contactdiv-new">
               <h3 className="contactnewheader">Contact Us</h3>
                <hr className="contactnewborder"/>
                <form>

                 <label className="textsize-contact">Name*</label>
                 <input type="text" name="fullname" className="form-control" placeholder="First and last name"/>


                  <label className="textsize-contact">Company*</label>
                 <input type="text" name="cname" className="form-control" placeholder="Company Name"/>
               
               
                  <label className="textsize-contact">E-mail*</label>
                 <input type="text" name="email" className="form-control" placeholder="Company e-mail"/>


                 <label className="textsize-contact">Phone*</label>
                 <input type="text" name="phone" className="form-control" placeholder="Phone"/>

                <FormGroup controlId="formControlsTextarea" >
                <ControlLabel className="textsize-contact">Comments*</ControlLabel>
                <FormControl className="textarea" componentClass="textarea"  name ="message"  />
                </FormGroup>
               </form>
              
              </div>
              <div className="success cnt-success ">
             <Button type="button" className="btn-contact" bsStyle="" bsSize="large" style={{paddingLeft:'50%',paddingRight:'50%', marginTop: '22%',background:'#4dac4c',color:'white'}}> Submit</Button>
              </div>

                
          </div>
        </div>
      </div>
      
      );
}
}
export default Contactnew;