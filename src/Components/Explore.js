import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Col, Form, FormGroup, Tab, ControlLabel, FromGroup, FormControl, Button, Image, Tabs } from 'react-bootstrap';
import { withGoogleMap, GoogleMap } from 'react-google-maps';
import { ReactSelectize, SimpleSelect, MultiSelect } from 'react-selectize';
import $ from 'jquery';
var selected_subservices = [];
const selectservice = [];
var subservice_array = [];
const service_array = [];
const location_array = [];
class Explore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ServicesData: {},
      serviceArea: [],
      selectservice: [],
      addselectsubservice: [],
      location: [],
      bttondisabled: true,
      allsubservices: []
    };
    this.serviceAreas = this.serviceAreas.bind(this);
    this.addselectservice = this.addselectservice.bind(this);
    this.test = this.test.bind(this);
    this.Submit = this.Submit.bind(this);
  }

  getExploreServicesData() {
    $.ajax({
      url: '/exploreServicesData.json',
      dataType: 'json',
      cache: false,
      success: function (data) {
        this.setState({ ServicesData: data });
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount() {
    this.servicearea();
    this.initMap();
    this.serviceAreas();
    setTimeout(function () {
      $('ul.nav-tabs li a').attr('href', '');
      $('.nav-tabs li a').css('display', 'none');
      $('#myBtn').css('visibility', 'hidden');
    }, 1500);
    subservice_array = [];
    this.getExploreServicesData();

  }



  servicearea() {
    window.onmousemove = function () {

      var anchors = document.getElementsByClassName('servicebox');

      for (var i = 0; i < anchors.length; i++) {
        var anchor = anchors[i];
        anchor.onclick = function () {

          var id = this.id;
          var str = id.slice(3, 5);
          str = parseInt(str) - 1;
          //alert(str);
          var element = document.getElementById(id);
          element.classList.toggle("slct_class");
          element.classList.toggle("unslct_class");
          var slct_class = document.getElementsByClassName('slct_class');
          if (slct_class.length > 0) {
            document.getElementsByClassName('subservice-area')[0].style.display = 'block';
          }
          else {
            document.getElementsByClassName('subservice-area')[0].style.display = 'none';
          }

          if (element.classList.contains('slct_class')) {
            //                    alert('id-tab-'+str);
            console.log("vishal", str);
            // $('.nav.nav-tabs > li > a').hide(); 
            // alert("a");

            // document.getElementById('id-tab-'+str).style.display = 'block';
            // document.getElementsByClassName('nav-tabs').style.display = 'block';
            document.getElementById('id-pane-' + str).style.display = 'block';
            document.getElementById("id-pane-" + str).setAttribute(
              "style", "color:#4dac4c;font-size:15px");
            document.getElementById("id-tab-" + str).setAttribute(
              "style", "color:#4dac4c;font-size:15px");
            document.getElementById("myBtn").style.visibility = 'visible';
            console.log(' $("#id-tab-' + str + '").css("display","block")');
            $("#id-tab-" + str).css("display", "block");
            // document.getElementById("myBtn").style.display='none';
            // var ul = document.getElementsByClassName("nav-tabs");
            // for (var i = 0; i < ul[0].childNodes.length; i++) {
            //     ul[0].childNodes[i].classList.remove("active");
            // }
            // var ul = document.getElementsByClassName("tab-pane");
            // for (var i = 0; i < ul.length; i++) {
            //     ul[i].classList.remove("active");
            //     ul[i].classList.remove("in");
            // }
            // document.getElementById('id-tab-'+str).parentElement.classList.add("active");
            // document.getElementById('id-pane-'+str).classList.add("active");
            // document.getElementById('id-pane-'+str).classList.add("in");
            $("#id-tab-" + str)[0].click();
          }
          else {

            document.getElementById('id-tab-' + str).style.display = 'none';
            document.getElementById('id-pane-' + str).style.display = 'none';
            var clicked_pen = $(".tab-content .tab-pane:visible");
            console.log(clicked_pen.length);
            if (clicked_pen.length == 0) {
              if ($(".nav.nav-tabs li:visible").children('a').length > 0) {
                $(".nav.nav-tabs li:visible").children('a')[0].click();
              }
            }
            document.getElementById("myBtn").style.visibility = 'hidden';
            var el = $("#id-pane-" + str + " .boxsubservice");
            el.removeClass('slct_class1');

            //   document.getElementsByClassName('nav-tabs').style.display = 'none';
          }


        }
      }

      var anchors1 = document.getElementsByClassName('boxsubservice');
      for (var i = 0; i < anchors1.length; i++) {
        var anchor = anchors1[i];
        anchor.onclick = function () {
          var slct_class = document.getElementsByClassName('slct_class1');
          var id = this.id;
          var element = document.getElementById(id);
          element.classList.toggle("slct_class1");
          element.classList.toggle("unslct_class");

        }
      }
    }
  }
  initMap() {



    var map = new window.google.maps.Map(document.getElementById('automap'), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13
    });

    var input = document.getElementById('pac-input');

    var autocomplete = new window.google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(
      ['address_components', 'geometry', 'icon', 'name']);

    var infowindow = new window.google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new window.google.maps.Marker({
      map: map,
      anchorPoint: new window.google.maps.Point(0, -29)
    });
    autocomplete.addListener('place_changed', (event) => {
      this.test(autocomplete.getPlace());
    });
    autocomplete.addListener('place_changed', function () {
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindowContent.children['place-icon'].src = place.icon;
      infowindowContent.children['place-name'].textContent = place.name;
      infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    });
  }
  addselectservice(event) {
    console.log("akki", this.state.selectservice);
    var new_array = this.state.selectservice;
    if (!new_array.includes(event)) {
      new_array.push(event);
      service_array.push(event);
      this.setState({ selectservice: new_array })

      this.state.serviceArea.map((expservice, index) => {
        if (expservice.service_area == event) {
          var last_services = expservice.services;
          last_services.map((ser) => {

            if ($.inArray(ser.service, selected_subservices) !== -1) {

            }
            else {
              selected_subservices.push(ser.service);
            }

            this.setState({ allsubservices: selected_subservices })
          })
        }
      })
    }
    else {
      new_array.splice(new_array.indexOf(event), 1);
      service_array.splice(service_array.indexOf(event), 1);
      this.setState({ selectservice: new_array })

      this.state.serviceArea.map((expservice, index) => {
        if (expservice.service_area == event) {
          var last_services = expservice.services;
          last_services.map((ser) => {

            if ($.inArray(ser.service, selected_subservices) !== -1) {
              selected_subservices.splice(selected_subservices.indexOf(ser.service), 1);
            }
            else {

            }

            this.setState({ allsubservices: selected_subservices })
          })
        }
      })
    }
    if (service_array.length > 0 && this.state.addselectsubservice.length > 0 && typeof (this.state.location[0]) != "undefined") {
      this.setState({ bttondisabled: false });
    }
    else {
      this.setState({ bttondisabled: true });
    }
  }
  addselectsubservice(event) {
    console.log("vishalmelwani", subservice_array);

    if (!subservice_array.includes(event)) {
      subservice_array.push(event);
      this.setState({ addselectsubservice: subservice_array })
    }
    else {
      subservice_array.splice(subservice_array.indexOf(event), 1);
      this.setState({ addselectsubservice: subservice_array })
    }
    if (subservice_array.length > 0 && this.state.selectservice.length > 0 && typeof (this.state.location[0]) != "undefined") {
      this.setState({ bttondisabled: false });
    }
    else {
      this.setState({ bttondisabled: true });
    }
  }
  test(place) {

    if (typeof (place.geometry) != 'undefined') {
      location_array.push(place.geometry.location.lng());
      location_array.push(place.geometry.location.lng());
      this.setState({ location: location_array });
      console.log(this.state);
      if (this.state.selectservice.length > 0 && this.state.addselectsubservice.length > 0 && location_array.length > 0) {
        this.setState({ bttondisabled: false });
      }
      else {
        this.setState({ bttondisabled: true });
      }
    }
  }
  serviceAreas() {
    $.ajax({
      url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/admin/api/v1/options/service-areas',
      dataType: 'json',
      type: 'GET',
      cache: false,
      success: function (data) {
          this.setState({ serviceArea: data['service_areas']});

      }.bind(this),
      error: function (xhr, status, err) {

      }
  });

  }
  Submit(e) {

    if (!this.state.bttondisabled) {
      $('.spanner').show();
      $.ajax({
        type: 'POST',
        mode: 'no-cors',
        headers: {
          'Content-Type': "application/json",
          'Access-Control-Allow-Origin': '*',
        },
        data: JSON.stringify({ "location": { "latitude": this.state.location[0], "longitude": this.state.location[1] }, "service_areas": this.state.selectservice, "services": this.state.addselectsubservice }),
        url: 'https://mycluster-328113.us-south.containers.appdomain.cloud/mece/api/v1/search',
        success: function (response) {
          console.log(response);
          console.log(response.companies);
          var new_array = [];
          new_array['response'] = response;
          new_array['services'] = this.state.selectservice;
          new_array['subservices'] = this.state.addselectsubservice;
          new_array['allsubservices'] = this.state.allsubservices;
          this.props.history.push({
            pathname: '/firm_list',
            data: new_array // your data array of objects
          })
          $('.spanner').hide();
          //this.props.onUpdate(response.companies);
        }.bind(this),
        error: function (xhr, status, err) {
          console.log(err);
          if (xhr.status == 503) {
            alert("Please try again. No match found.");
            //window.location.reload();
          }
          $('.spanner').hide();
        }.bind(this)

      });
    }
    else {
      this.setState({ bttondisabled: true });
    }

  }
  render() {

    if (this.state.ServicesData.exp) {
      var j = 1;
      var expservice = this.state.serviceArea.map((expservice, index) => {

        return <div className="col-md-3 col-sm-6">
          <div id={"box" + j} onClick={() => this.addselectservice(expservice.service_area)} className={"box" + j++ + " servicebox unslct_class"}>
            <div className="overlay-explore">
              <strong><i className="fa fa-check" style={{ fontSize: "50px", color: 'black' }}></i></strong>
            </div>
            <p className="box-p1">{expservice.service_area}</p>
          </div>
        </div>
      })
      var i = 0;
      var myservice = this.state.serviceArea.map((myservice, index) => {

        return (<Tab eventKey={index} title={myservice.service_area}>
          <div className="col-md-12 sales-content">
            {myservice.services.map((item) => {

              return <div className="col-md-3" onClick={() => this.addselectsubservice(item.service)}>
                <div id={"boxsubservice" + i++} className="boxsubservice unslct_class">
                  <div className="overlay-explore">
                    <div className="text"><strong><i className="fa fa-check" style={{ fontSize: "50px", color: 'black' }}></i></strong></div>
                  </div>
                  <p className="box-subservice-p1">{item.service}</p>
                </div>
              </div>
            })}
          </div>
        </Tab>
        )

      })
    }

    return (
      <div class="container-fluid">

        <div className="col-md-12 col-sm-12 text-center">
          <h2>Select the services and location of your choice to view your options</h2>
        </div>

        <div>
          <div className="col-md-12 col-sm-12" style={{ marginBottom: '30px', cursor: 'pointer' }}>
            <div className="col-md-12 col-sm-12 speciality-area">
              <h2 className="specialityarea-h2">Speciality Area</h2>
              {expservice}
            </div>
          </div>
        </div>

        <div>
          <div className="col-md-12 col-sm-12" style={{ marginBottom: '30px', cursor: 'pointer' }}>
            <div className="col-md-12 col-sm-12 subservice-area" style={{ display: 'none' }}>
              <h2 className="subservice-h2">Subservices</h2>
              <Tabs id="id">
                {myservice}

              </Tabs>
            </div>
          </div>
        </div>
        <button id="myBtn" title="Scroll up" ></button>


        <div className="explore-form-div ">
          <form className="explore-search-form">
            <Col md={12} xs={12} className="main-explore-div">
              {/*
      <Col md={4} sm={6} xs={12}>
      <FormGroup>
      <FormControl className="exp-keyword-first" type="text" placeholder="Keyword Search" />
      </FormGroup>
      </Col>
      */}

              <Col md={6} sm={6} xs={12}>
                <FormGroup>
                  <FormControl className="exp-keyword-sec" type="text" placeholder="Location" id="pac-input" />
                </FormGroup>
              </Col>

              <Col xs={12} md={6}>
                <Button className="btn-lg explr-search-btn" bsStyle="" disabled={this.state.bttondisabled ? 'disabled' : ''} onClick={this.Submit}>Search</Button>
              </Col>
            </Col>

          </form>
        </div>


        <div id="automap" style={{ height: '500px', position: 'relative', top: '20%', width: '100%' }}></div>
        <div id="infowindow-content">
          <img src="" width="16" height="16" id="place-icon" />
          <span id="place-name" class="title"></span><br />
          <span id="place-address"></span>
        </div>
      </div>

    );
  }
}
export default Explore;