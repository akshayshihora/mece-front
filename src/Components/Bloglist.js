import React from 'react';
import { Col, Image, Button } from 'react-bootstrap';
import Pagination from "react-js-pagination";
import { Link } from "react-router-dom";
import $ from 'jquery';
import ReactGA from 'react-ga';


class Bloglist extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			bloglistData: {}
		};
		this.createMarkup = this.createMarkup.bind(this);
		// this.myFunction = this.myFunction.bind(this);
		ReactGA.initialize('UA-110570651-1');
		ReactGA.pageview(window.location.pathname);

	}

	getBloglistData() {
		$.ajax({
			url: '/bloglistData.json',
			dataType: 'json',
			cache: false,
			success: function (data) {
				this.setState({ bloglistData: data });
			}.bind(this),
			error: function (xhr, status, err) {
				console.log(err);
				alert(err);
			}
		});
	}

	componentDidMount() {
		this.getBloglistData();
		$("#readmore_6").siblings(".bloglist-desciption").find(".dots").hide();
	}
	createMarkup(text) {
		return { __html: text };
	};

	myFunction(index) {
		// alert('as');
		// console.log("as",index);

		//var dots = $("#readmore_"+index).parent(".blog-list-div").find(".bloglist-desciption").find(".dots");
		//	var moreText = $("#readmore_"+index).parent(".blog-list-div").find(".bloglist-desciption").find(".more");
		//console.log(dots.length);
		//console.log(moreText);

		//  $("#readmore_"+index).siblings(".bloglist-desciption").find(".dots").toggleClass("dot_hide");
		//  $("#readmore_"+index).siblings(".bloglist-desciption").find(".more").toggleClass("more_show");
		// var btnText = document.getElementById("readmore_"+index);

		// if (dots.style.display === "none") {
		// 	dots.style.display = "inline";
		// 	btnText.innerHTML = "Read more"; 
		//  	moreText.style.display = "none";
		// } else {
		// 	dots.style.display = "none";
		//  	btnText.innerHTML = "Read less"; 
		//  	moreText.style.display = "inline";
		// 	}

		var dots = $("#maindiv_" + index + " .dots");
		var moreText = $("#maindiv_" + index + " .more");
		var btnText = $("#readmore_" + index);

		if (dots.css('display') === "none") {
			dots.css('display', "inline");
			btnText.html("Read more");
			moreText.css('display', "none");
		} else {
			dots.css('display', "none");
			btnText.html("Read less");
			moreText.css('display', "inline");
		}

	}

	render() {
		//  var index = 0;

		if (this.state.bloglistData.blog) {
			var bloglist = this.state.bloglistData.blog.bloglist.map((bloglist, index) => {

				var checkBloglistImage = bloglist.image == "" ? "photo2.png" : bloglist.image;
				var bloglistImage = '/images/blog-list-image/' + checkBloglistImage;
				index = index + 1;
				return <div className="col-md-12 bloglist-main" style={{ boxShadow: 'rgba(0, 0, 0, 0.16) 0px 2px 5px 0px, rgba(0, 0, 0, 0.12) 0px -5px 10px 0px' }}>
					<div className="col-md-4">
						{/* <img className="blg-list-img" src={bloglistImage} /> */}
						<div className="blg-list-img" style={{ backgroundImage: "url(" + bloglistImage + ")" }}></div>

					</div>

					<div className="col-md-8 blog-list-div" id={"maindiv_" + index}>
						<h1><p>{bloglist.title}</p>
						</h1>
						{/* <p className="bloglist-p">{bloglist.date} | Category: <span className="span-blog">{bloglist.category} </span>|&nbsp;<span className="span-blog">{bloglist.comment}</span></p> */}
						<p className="bloglist-desciption" dangerouslySetInnerHTML={{ __html: bloglist.description }}></p>

						{/* <Link to="/blog" className="blog-list-more">Read more</Link> */}

						{/* <button onClick={()=>this.myFunction(index)} id={"readmore_"+index} className="blog-list-more readmore_btn">Read more</button> */}

						<Link to={{ pathname: '/blog', state: { 'bloglistData': bloglist } }} className="text sidebar-list-more" >

							More Details</Link>
					</div>
				</div>


			})

		}

		return (

			<div className="container">
				<div className="row">
					<div className="col-md-12 bloglist-main">
						{bloglist}

					</div>
				</div>
			</div>
		);
	}
}
export default Bloglist;
