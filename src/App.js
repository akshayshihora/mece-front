
import React, { Component } from 'react';
import ReactGA from 'react-ga';
import $ from 'jquery';
import './App.css';
import firm_enquiry from './Components/firm_enquiry';
import firm_list from './Components/firm_list';
import Footer from './Components/Footer';
import Companydetail from './Components/Companydetail';
import Demo from './Components/Demo';
import Blog from './Components/Blog';
/*import Header from './Components/Header';*/
import Signup from './Components/Signup';
import Login from './Components/Login';
import Home from './Components/Home';
import Bloglist from './Components/Bloglist';
import Try from './Components/Try';
import Terms from './Components/Terms';
import Explore from './Components/Explore';
import Reference from './Components/Reference';
import Thankyou from './Components/Thankyou';
import { HashLink as Link } from 'react-router-hash-link';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import {Navbar, Form, Col, FormGroup, ControlLabel, FormControl, Button, Nav, MenuItem, NavDropdown, NavItem, Image} from 'react-bootstrap';
import SocialMediaIcons from 'react-social-media-icons';
import { InputGroup, InputGroupText, InputGroupAddon, Input } from 'reactstrap';
import Scroll from 'smooth-scroll';
import Contact from './Components/Contact';
import ScrollToTop from 'react-router-scroll-top';

import Main from './Components/Main';


const socialMediaIcons = [
  {
  url: 'https://www.linkedin.com/company/mece',
  className: 'fab fa-linkedin-in footer-soc',
  },
  {
  url: 'https://twitter.com/MECE_Inc',
  className: 'fab fa-twitter footer-soc',
  },

];

class App extends Component {



  constructor(props){
    super(props);
    
    this.state = {
      foo: 'bar',
      contactName:'',
      contactEmail:'',
      contactSubject:'',
      contactMessage:'',
      resumeData: {}
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  

  getResumeData(){
    $.ajax({
      url:'/resumeData.json',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({resumeData: data});
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount(){
        this.getResumeData();
        
  }


  componentDidMount() {
        // initiate smooth scrolling
    let scroll = new Scroll('a[href*="#"]');
    
  }


  render() {

    return (
          <Router>
        <div className="App">
            <div class="overlay"></div>
            <div class="spanner" style={{display:'none'}}>
              <div class="loader"></div>
            </div>
        <Main/>

        <Navbar inverse collapseOnSelect fixedTop>
      <Navbar.Header>
        <Navbar.Brand>
           <Link to="/Home"><Image src="./images/MECEwithoutinc.png" style={{width:'50%'}}/></Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
          <Nav className="main-nav" style={{fontWeight:'bold',color:'white',fontSize:'18px'}}>
             <NavItem eventKey={2}>
              <Link to="/home#top" className="link">Home</Link>
            </NavItem>
            <NavItem eventKey={1}  >
             <Link to="/explore#top" className="link">Explore</Link>
            </NavItem>
           
            <NavItem eventKey={2}>
               <Link to="/home#valuepropostion" className="link ">About</Link>
            </NavItem>
             <NavItem eventKey={2}>
               <Link to="/demo#top" className="link">Demo</Link>
            </NavItem>
            <NavItem eventKey={2}>
              <Link to="/bloglist#top" className="link">Blog</Link>
            </NavItem>
            
            <NavItem eventKey={2} >
             <Link to="/contact#top" className="link">Contact</Link>
            </NavItem>
            
            
          </Nav>
          <Nav pullRight style={{fontSize:'18px'}}>

            <NavItem eventKey={1} >
            
            <Image src="./images/key.png" style={{width:'19px'}} /> &nbsp; 
             <Link to="/signup" className="link">Signup</Link>
            </NavItem>
            <NavItem eventKey={2}>
            <Image src="./images/login.png" style={{width:'19px'}} /> &nbsp;
              <Link to="/login" className="link">Login</Link>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

        
      
         <Route exact path="/" component={Home} />
         <Route path="/firm_list" component={firm_list} />
         <Route path="/Home" component={Home} />
         <Route path="/Companydetail" component={Companydetail} />
         <Route path="/Bloglist" component={Bloglist} />
         <Route path="/Demo" component={Demo} />
         <Route path="/Blog" component={Blog} />
         <Route path="/firm_enquiry" component={firm_enquiry} />
         <Route path="/Login" component={Login} />
         <Route path="/Signup" component={Signup} />
         <Route path="/Try" component={Try} />
         <Route path="/Explore" component={Explore} />
         <Route path="/Terms" component={Terms} />
         <Route path="/Reference" component={Reference} />
         <Route path="/Thankyou" component={Thankyou} />
         <Route path="/Contact" component={Contact} />

    <div style ={{backgroundColor: '#030542'}} className="row footer-div">
    <div className = "container">
    
     <div class="col-md-3 footer-left-div">
      <Link to="/Home"><Image style={{height:'30px'}} className="logo" src="./images/MECEwithoutinc.png" /></Link>
      <p className="footer-left-div-p">10601 Pecan Park Blvd #202, <br /> Austin, TX 78750 <br/>Info@meceinc.com <br/>(304) 933 – 9384</p>
     </div>

    <div class="col-md-7 footer-center-div">
     <ScrollToTop>
    <div className="col-md-12">
      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/Home" className="link">Home</Link>&nbsp;
      </div>

      <div className="col-sm-2 col-xs-12 footer-menu">
      <Link to="/Explore" className="link">Explore</Link>&nbsp;
      </div>
      
      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/Home#valuepropostion" className="link">About</Link>&nbsp;
      </div>

      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/Demo" className="link">Demo</Link>&nbsp;
      </div>

      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/Bloglist" className="link">Blog</Link>&nbsp;
      </div>
      
      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to ="/Contactnew" className="link">Contact</Link>&nbsp;
      </div>
      
    </div>
     </ScrollToTop>
   
    <div className="subscribe-div">
    <p> Subscribe Us </p>
    <InputGroup className="footer-email-group">
    <Input className="inpt-text footer-email" placeholder="Email Address" style={{color:'white'}}/>
      <a href="javascript:void(0)"><InputGroupAddon addonType="append" style={{zIndex:'11219', position:'relative',float:'right', marginTop: '-39px',padding:'2px 1px'}}>
      <InputGroupText className='btn' style={{fontSize:'16px',borderTop: '1px solid #4dac4c', backgroundColor: '#4dac4c',color:'white'}}><i class="fa fa-angle-right" aria-hidden="true"></i></InputGroupText>
      </InputGroupAddon></a>
    </InputGroup>
    </div>
   </div>

    <div class="col-md-2 footer-right-div">
    <div className="text-center">
    <p style={{color:'white',fontWeight:'bold',fontSize:'15px'}}> Follow Us </p>
      <SocialMediaIcons
        icons={socialMediaIcons}
        iconSize={'1.3em'}
        iconColor={'#495056'}
      />

    </div>
    </div>
    <div className='copyright-div text-center'>
    <p>Copyright {(new Date().getFullYear())}, MECE All Right Reserved </p>
    </div> 
      <div className="terms-ser-div">
        <Link to="/Terms" className="link">Terms of services </Link>
      </div>
    </div>
    </div>
    </div>
    
    </Router>
    );
  }
}
export default App;
