import React, { Component } from 'react';
import {Navbar, Nav, MenuItem, NavDropdown, NavItem,Button,SplitButton,Form,Col,Image} from 'react-bootstrap';
import Select from 'react-select';
import {Multiselect} from 'react-bootstrap-multiselect';
import { colourOptions } from '../data';
import { BrowserRouter, Route } from 'react-router-dom';

class Main extends Component {

  render() {


      if(window.location.pathname == '/Home')
      {
        return (
        <div style ={{ backgroundImage: "url(./images/banner-img.jpg)", height: '635px'}} className="header-div" >

        </div> );
      }
      else if(window.location.pathname == '/About')
      {
        return (
        <div style ={{ backgroundImage: "url(./images/banner-img.jpg)" }} className="header-div" >

        </div> );
      }

      else if(window.location.pathname == '/Sidebar')
      {
        return (
        <div style ={{ backgroundImage: "url(./images/Explore.png)", height: '320px'}} className="header-div" >

        </div> );
      }
      else if(window.location.pathname == '/Blog')
      {
        return (

        <div style ={{ backgroundImage: "url(./images/blog1.png)", height: '320px'}} className="header-div" >

        </div> );
      }
      else if(window.location.pathname == '/Reg')
      {
        return (
        <div style ={{ backgroundImage: "url(./images/CONTACT-FIRM.png)", height: '320px'  }} className="header-div" >
          <h1 style={{color:'white', marginTop:'0px', paddingTop:'10%', textAlign:'center',lineHeight:'47px', fontWeight:'600'}}> Provide details to help firms propose the <br /> best-in-class solutions for your need </h1>
        </div> );
      }
      else if(window.location.pathname == '/Companydetail')
      {
        return (
        <div style ={{ backgroundImage: "url(./images/company-details.png)", height: '340px', position: 'relative'}} className="header-div" >

        </div> );
      }
      else
      {
        return (
        <div style ={{ backgroundImage: "url(./images/banner-img.jpg)" }} className="header-div" >

        </div> );
      }
  }
}

export default Main;
