<?php 


if ($_SERVER["REQUEST_METHOD"] == "POST") {

$name = strip_tags(trim($_POST["contactName"]));

$email = strip_tags(trim($_POST["contactEmail"]));

$subject = strip_tags(trim($_POST["contactSubject"]));

$message = strip_tags(trim($_POST["contactMessage"]));

// Set the recipient email address.
// FIXME: Update this to your desired email address.
$recipient = "aspltest3@gmail.com";

// Set the email subject.
$subject = "New contact from $name Via React Site";

// Build the email content.
$email_content = "Name: $name\n";
$email_content .= "Email: $email\n\n";
$email_content .= "Message:\n$message\n";

// Build the email headers.
$email_headers = "From: $name <$email>";

// Send the email.
if (mail($recipient, $subject, $email_content, $email_headers)) {
// Set a 200 (okay) response code.

echo "Thank You! Your message has been sent.";
} else {
// Set a 500 (internal server error) response code.

echo "Oops! Something went wrong and we couldn’t send your message.";
}

} else {
// Not a POST request, set a 403 (forbidden) response code.

echo "There was a problem with your submission, please try again.";
}


?>